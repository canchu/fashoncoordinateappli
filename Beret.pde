class Beret{
  float x;
  float y;
  float yoko;
  float tate;
  
  Beret(int hx,int hy){
    x    = hx*193/200;
    y    = hy*120/300;
    yoko = 61;
    tate = 55;
  }
  
  void s_display(int R, int G, int B, int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    
    /*draw the cap*/
    quad(x+10, y-20, x+16, y-35, x+18, y-34, x+16, y-20);
    quad(x-17, y-23, x-18, y-20,x+28, y-8, x+18, y-25);
    quad(x-18, y-18, x-20, y-14, x+30, y-5, x+29, y-10);
    arc(x+8.5, y-17, yoko*0.9, tate*0.5, -PI*3/3, PI*1.5/6, PIE);
    noStroke();
    quad(x-15, y-23, x-5, y-17, x+18, y-11, x+15, y-25);
    
    Gara(pattern);
     
    
  }
    void Gara(int pattern){
    Pattern p1;
    switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x+10,(int)y-30,50,20);
         p1.b_display(4,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x+10,(int)y-30,50,20);
         p1.s_display(4,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x+20,(int)y-30,20,20);
         p1.f_display(4,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x+20,(int)y-30,20,20);
         p1.h_display(4,1);
         break;
      default:
        break;
    }
  }

}
