//written by Ayano

import ddf.minim.*;
Minim minim;

//main function and define global variables
int          gPageNumber;
int          gClothNumber;
boolean      gKeyRelease;
ColorPalette Palette;
AudioSnippet sound;
int          aClothTextNumber[] = new int[200];

int          l;//using in Coordinate_SelectClothes

PImage top;

void setup(){
  size(800,600);
  strokeWeight(1);
  smooth();
  ellipseMode(CENTER);
  textMode(CENTER);
  rectMode(CENTER);
  imageMode(CENTER);
  
  gPageNumber   = 0;
  gSelectNumber = 0;
  gHieralcy     = 0;
  Palette       = new ColorPalette();
  minim         = new Minim(this);
  l             = 0;
  top           =loadImage("top.png");//Written by Natsuko
  img2          =loadImage("m1.png");
  img3          =loadImage("kabe5-1.png");
  img4          =loadImage("tokei2.png");
  img5          =loadImage("mado5.png");  
  img6          =loadImage("table1.png");
  img7          =loadImage("kuma3.png");  
  img8          =loadImage("sen2.jpg");
  img9          =loadImage("3.png");
  Coordiimg2    =loadImage("9.png");
  Coordiimg3    =loadImage("2.jpg");
}

void draw(){ //Main Function
  background(255);
  String aNumber[] = loadStrings("TextNumber.txt");
  String aBuff = aNumber[0];
  gTextNumber = Integer.parseInt(aBuff);
  
  switch(gPageNumber){
    case 0:
      Title();
    break;
   
    case 1:
      Select();
    break;
    
    case 2:
      Coordinate();
    break;
   
   case 3:
      Delete();
   break;
   
    default:
    break;
  }
  
  
  if(!keyPressed){
    gKeyRelease = false;
  }
}

void mousePressed(){
  Palette.mousePressed();
}

void stop(){
  sound.close();
  minim.stop();
  super.stop();
}

void keyReleased(){
  gKeyRelease = true;
}

  
