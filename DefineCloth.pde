//Written by Yoshiko and Ayano

void DefineCloth(int pattern,int type1,int type2,int type3,int type4,int r,int g,int b,int ClothX,int ClothY){
  
    switch (type1){
      case 1:  
        switch (type2){  
          case 1:
            switch (type4){
              case 1:
                T_shirt NagasodeTshirt = new T_shirt(ClothX,ClothY);
                NagasodeTshirt.l_display(r,g,b,pattern);
              break;
              case 2:          
                T_shirt HansodeTshirt = new T_shirt(ClothX,ClothY);
                HansodeTshirt.s_display(r,g,b,pattern);
              break;
              default:
              break;
            }
         break;
         case 2:
          switch (type4){
            case 1:              
              if(type3==2){
                Blouse NagaEriBlouse = new Blouse(ClothX,ClothY);
                NagaEriBlouse.eri_l_display(r,g,b,pattern);
              }else{
                Blouse NagaBlouse = new Blouse(ClothX,ClothY);
                NagaBlouse.l_display(r,g,b,pattern);
              }
              break;
            case 2:            
              if(type3==2){
                 Blouse HanEriBlouse = new Blouse(ClothX,ClothY);
                 HanEriBlouse.eri_s_display(r,g,b,pattern);
              }else{
                 Blouse HanBlouse = new Blouse(ClothX,ClothY);
                 HanBlouse.s_display(r,g,b,pattern);
              }
              break;
            default:
              break;
          }

          break;
        case 3:
          if(type3==2){
            Knit EriKnit = new Knit(ClothX,ClothY);
            EriKnit.eri_l_display(r,g,b,pattern);
          }
          else{
            Knit knit = new Knit(ClothX,ClothY);
            knit.l_display(r,g,b,pattern);
          }
          break;
        case 4:
          Cardigan cardigan = new Cardigan(ClothX,ClothY);
          cardigan.l_display(r,g,b,pattern);
        break;
        case 5:
          switch (type4){
            case 1:
              Parka NagaParka = new Parka(ClothX,ClothY);
              NagaParka.l_display(r,g,b,pattern); 
              break;
            case 2:
              Parka HanParka = new Parka(ClothX,ClothY);
              HanParka.s_display(r,g,b,pattern);
              break;
            default:
              break;
          }
         break;

        default:
          break;
      }
      break;
    case 2:
      switch (type2){  
        case 1:
          Tailored tailored = new Tailored(ClothX,ClothY);
          tailored.l_display(r,g,b,pattern);
          break;
        case 2:
          Coat coat = new Coat(ClothX,ClothY);
          coat.l_display(r,g,b,pattern);
          break;
        case 3:
          Trench trench = new Trench(ClothX,ClothY);
          trench.l_display(r,g,b,pattern);
          break;
        case 4:
          Poncho poncho = new Poncho(ClothX,ClothY);
          poncho.l_display(r,g,b,pattern);
          break;
  
        default:
          break;
      }
      
      break;
    case 3:
      switch (type2){
        case 1:
          Pants LongPants = new Pants(ClothX,ClothY);
          LongPants.l_display(r,g,b,pattern);
          break;
        case 2:
          Pants ShortPants = new Pants(ClothX,ClothY);
          ShortPants.s_display(r,g,b,pattern);
          break;
        default:
          break;
      }
      break;
    case 4:
      switch (type2){
        case 1:
          Long_Skirt LongSkirt = new Long_Skirt(ClothX,ClothY);
          LongSkirt.l_display(r,g,b,pattern);
          break;
        case 2:
          Short_Skirt ShortSkirt = new Short_Skirt(ClothX,ClothY);
          ShortSkirt.s_display(r,g,b,pattern);
          break;
        default:
          break;
      }

      break;
    case 5:
      switch (type2){
        case 1:
          switch (type4){
            case 1:
              if(type3 == 1){
                One_piece_Dress NagaOnePiece = new  One_piece_Dress(ClothX,ClothY);
                NagaOnePiece.l_display(r,g,b,pattern);
              }
              else{
                 One_piece_Dress NagaEriOnePiece = new  One_piece_Dress(ClothX,ClothY);
                 NagaEriOnePiece.eri_l_display(r,g,b,pattern);
              }
              break;
            case 2:
              if(type3 == 1){
                One_piece_Dress HanOnePiece = new  One_piece_Dress(ClothX,ClothY);
                HanOnePiece.s_display(r,g,b,pattern);
              }
              else{
                 One_piece_Dress HanEriOnePiece = new  One_piece_Dress(ClothX,ClothY);
                 HanEriOnePiece.eri_s_display(r,g,b,pattern);
              }
              break;
            default:
              break;
          }
          break;
        case 2:
          switch (type4){
            case 1:
              Tunic NagaTunic = new Tunic(ClothX,ClothY);
              NagaTunic.l_display(r,g,b,pattern);
              break;
            case 2:
              Tunic HanTunic = new Tunic(ClothX,ClothY);
              HanTunic.s_display(r,g,b,pattern);
              break;
            default:
              break;
          }

          break;
        default:
          break;
      }

      break;
    case 6:
      switch (type2){
        case 1:
          Sneaker sneaker = new Sneaker(ClothX,ClothY);
          sneaker.s_display(r,g,b,pattern);
          break;
        case 2:
          Pumps p1 = new Pumps(ClothX,ClothY);
          p1.s_display(r,g,b,pattern);
          break;
        case 3:
          Boots ShortBoots = new Boots(ClothX,ClothY);
          ShortBoots.s_display(r,g,b,pattern);
          break;
        case 4:
          Boots LongBoots = new Boots(ClothX,ClothY);
          LongBoots.l_display(r,g,b,pattern);
          break; 
        default:
          break;
      }
      
      break;
    case 7:
      switch(type2){
        case 1:
          Knit_Cap KnitCap = new Knit_Cap(ClothX,ClothY);
          KnitCap.s_display(r,g,b,pattern);
        break;
        case 2:
           Beret beret = new Beret(ClothX,ClothY);
           beret.s_display(r,g,b,pattern);
        break;
        default:
        break;
      }
    break;
    case 8:
      switch (type2){
        case 1:
          Tights tights = new Tights(ClothX,ClothY);
          tights.l_display(r,g,b,pattern);
          break;
        case 2:
          Socks KneeSocks = new Socks(ClothX,ClothY);
          KneeSocks.l_display(r,g,b,pattern);
          break;
        case 3:
          Socks NormalSocks = new Socks(ClothX,ClothY);
          NormalSocks.s_display(r,g,b,pattern);
          break;  
        default:
          break;
       
      }
      break;
    default:
      break;    
  }
}







