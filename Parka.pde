//Written by Amai
class Parka{
  float x;
  float y;
  float yoko;
  float tate;
  
  Parka(int hx,int hy){
    x    = hx*195/200;
    y    = hy*226/300;
    yoko = 90;
    tate = 110;
  }
  
  void s_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Parka*/
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    arc(x-((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,HALF_PI,PI);
    arc(x+((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,0,HALF_PI);
    rect(x+1.5,y+(tate/2-tate/5+yoko/20),(yoko*3/5),yoko/10);
    line(x+1.5-((yoko*3/5)/2),y+(tate/2-tate/5+yoko/20),x+((yoko*3/5)/2),y+(tate/2-tate/5+yoko/20));
    arc(x+1.5,y+(tate/2-tate/10-yoko/20+0.5),(yoko*3.1/5),yoko/10,0,PI);
    
    //draw the arms
    quad(x-yoko*3/10,y-tate/2,x-yoko*4/10,y-tate*4.5/10,x-yoko/2,y-tate*3/10,x-yoko*3/10,y-tate*2/10);
    quad(x+yoko*3/10,y-tate/2,x+yoko*4/10,y-tate*4.5/10,x+yoko/2,y-tate*3/10,x+yoko*3/10,y-tate*2/10);
    
    
    /*disappear the lines*/
    stroke(R,G,B);
    //disappear lines of the shoulder
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-6));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-5));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear lines of the parka
    rect(x,y+(tate/2-tate/5-yoko/20+yoko*1.5/40),(yoko*3.2/5),yoko/10);
   
    /*draw laces*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko/10,y-tate*2.5/8,yoko/8,tate/5,HALF_PI*1.7,HALF_PI*3);
    arc(x+yoko/10,y-tate*2.5/8,yoko/8,tate/5,-HALF_PI,HALF_PI*0.3);
    ellipse(x-yoko*1.6/10,y-tate*2.0/8,2,2);
    ellipse(x+yoko*1.6/10,y-tate*2.0/8,2,2);
    
    /*disappear the neck*/
    fill(254,237,217);
    triangle(x-yoko/7,y-(tate/2+0.5),x+yoko/7,y-(tate/2+0.5),x,y-tate*40/95);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko/7,y-(tate/2+0.5),yoko*2/5,tate/5,PI/4,PI*4.5/4);      //left
    arc(x-yoko*0.85/5,y-tate/2,yoko/3,tate/7,-PI*3.5/4,-PI*1.25/4);
    line(x-yoko*0.8/7,y-(tate*40/95+tate/30),x,y-tate*40/95);
    arc(x+yoko/7,y-(tate/2+0.5),yoko*2/5,tate/5,-PI*0.5/4,PI*3/4);      //right
    arc(x+yoko*0.85/5,y-tate/2,yoko/3,tate/7,-PI*2.8/4,-PI*0.25/4);
    line(x+yoko*0.8/7,y-(tate*40/95+tate/30),x,y-tate*40/95);
    
    Gara(pattern);
  }
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Parka*/ 
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.1/2),x-(yoko*3.8/10),y+(tate*1.3/2),x-(yoko*2.5/10),y+(tate/4));
    arc(x-(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-4),yoko/4.3,yoko/5,PI*1.5/4,PI*4.5/4);
    quad(x-((yoko*5.5/10)-1),y+((tate*1.1/2)+5),x-((yoko*5.5/10)+2),y+((tate*1.1/2)+10),x-((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x-((yoko*3.8/10)+1),y+(tate*1.3/2));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.1/2),x+(yoko*3.8/10),y+(tate*1.3/2),x+(yoko*2.5/10),y+(tate/4));
    arc(x+(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-4),yoko/4.3,yoko/5,-PI*0.5/4,PI*2.5/4);
    quad(x+((yoko*5.5/10)-1),y+((tate*1.1/2)+5),x+((yoko*5.5/10)+2),y+((tate*1.1/2)+10),x+((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x+((yoko*3.8/10)+1),y+(tate*1.3/2));
    
    
    //draw body
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    arc(x-((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,HALF_PI,PI);
    arc(x+((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,0,HALF_PI);
    rect(x+1.5,y+(tate/2-tate/5+yoko/20),(yoko*3/5),yoko/10);
    line(x+1.5-((yoko*3/5)/2),y+(tate/2-tate/5+yoko/20),x+((yoko*3/5)/2),y+(tate/2-tate/5+yoko/20));
    arc(x+1.5,y+(tate/2-tate/10-yoko/20+0.5),(yoko*3.1/5),yoko/10,0,PI);
    
    
    /*disappear the lines*/
    stroke(R,G,B);
    //disappear lines of the shoulder
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-6));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-5));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear lines of the parka
    rect(x,y+(tate/2-tate/5-yoko/20+yoko*1.5/40),(yoko*3.2/5),yoko/10);
   
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+6),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+9),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+7),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+9),y+(tate/4));
    
   
    /*draw laces*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko/10,y-tate*2.5/8,yoko/8,tate/5,HALF_PI*1.7,HALF_PI*3);
    arc(x+yoko/10,y-tate*2.5/8,yoko/8,tate/5,-HALF_PI,HALF_PI*0.3);
    ellipse(x-yoko*1.6/10,y-tate*2.0/8,2,2);
    ellipse(x+yoko*1.6/10,y-tate*2.0/8,2,2);
    
    /*disappear the neck*/
    fill(254,237,217);
    triangle(x-yoko/7,y-(tate/2+0.5),x+yoko/7,y-(tate/2+0.5),x,y-tate*40/95);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko/7,y-(tate/2+0.5),yoko*2/5,tate/5,PI/4,PI*4.5/4);      //left
    arc(x-yoko*0.85/5,y-tate/2,yoko/3,tate/7,-PI*3.5/4,-PI*1.25/4);
    line(x-yoko*0.8/7,y-(tate*40/95+tate/30),x,y-tate*40/95);
    arc(x+yoko/7,y-(tate/2+0.5),yoko*2/5,tate/5,-PI*0.5/4,PI*3/4);      //right
    arc(x+yoko*0.85/5,y-tate/2,yoko/3,tate/7,-PI*2.8/4,-PI*0.25/4);
    line(x+yoko*0.8/7,y-(tate*40/95+tate/30),x,y-tate*40/95);
    
    Gara(pattern);
  }
  
    
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y-10),56,100);
         p1.b_display(1,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y-10),70,90);
         p1.s_display(1,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y-10),70,100);
         p1.f_display(1,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y-10),70,100);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
  
  
}
