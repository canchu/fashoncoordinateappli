//Written by Ayano

class ClothButton{
  int      mX;
  int      mY;
  int      mHeight;
  int      mWidth;
  String   mClothNumberText;
  color    mButtonColor;
  color    mButtonReverseColor;
  boolean  mButtonPressed;
  TextLoad ClothModel;
  
  ClothButton(int aButtonX,int aButtonY,String aClothNumberText,color aColor,color aReverseColor){
    mX                  = aButtonX;
    mY                  = aButtonY;
    mHeight             = 155;
    mWidth              = 100;
    mClothNumberText    = aClothNumberText;
    mButtonColor        = aColor;
    mButtonReverseColor = aReverseColor;
  }
  
  void Disp(){
    rectMode(CENTER);
    Is_Button_Pressed();
    
    ClothModel = new TextLoad(mClothNumberText);
    if(ClothModel.type2 == 3){
       ClothModel.Display(mX,mY-100);
    }
    else{
      ClothModel.Display(mX,mY);
    }
    
    if(mouseX > mX-mWidth/2  && mouseX < mX + mWidth/2 ){
      if(mouseY > mY-mHeight/2 && mouseY <mY + mHeight/2){
        stroke(mButtonReverseColor);
      }
      else{
         stroke(mButtonColor);
      }
    }
    else{
       stroke(mButtonColor);
    }
    noFill();
    if(gPageNumber!=0){
      rect(mX-10,mY-30,mWidth,mHeight);
    }
 }
  
  void Is_Button_Pressed(){
    if(mouseX > mX - mWidth/2 && mouseX < mX + mWidth/2){
      if(mouseY > mY - mHeight/2 && mouseY < mY + mHeight/2){
        if(mousePressed){
          sound = minim.loadSnippet("Button.mp3");
          sound.play();
          mButtonPressed = true;
        }
        else{
          mButtonPressed = false;
        }
      }
    }
    else{
      mButtonPressed = false;
    }
  }
  
}
