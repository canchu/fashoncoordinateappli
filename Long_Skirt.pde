//Written by Amai

class Long_Skirt{
  float x;
  float y;
  float yoko;
  float tate;
  
  Long_Skirt(int hx,int hy){
    x    = hx*196/200;
    y    = hy*375/300;
    yoko = 60;
    tate = 220;
  }
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw long_skirt*/
    rect(x,y,yoko*9/10,tate);
    triangle(x-(yoko*9/10)/2,y-tate/2,x-yoko/2,y-tate*4/10,x-(yoko*9/10)/2,y-tate*4/10);
    quad(x-yoko/2,y-tate*4/10,x-yoko*2/3,y+tate/2,x-(yoko*9/10)/2,y+tate/2,x-(yoko*9/10)/2,y-tate*4/10);
    triangle(x+(yoko*9/10)/2,y-tate/2,x+yoko/2,y-tate*4/10,x+(yoko*9/10)/2,y-tate*4/10);
    quad(x+yoko/2,y-tate*4/10,x+yoko*2/3,y+tate/2,x+(yoko*9/10)/2,y+tate/2,x+(yoko*9/10)/2,y-tate*4/10);
    arc(x,y+tate/2,yoko*2.6/2,yoko/7,0,PI);
    
    /*disappear the lines*/
    stroke(R,G,B);
    line(x-(yoko*9/10)/2,y+(tate/2-1),x-(yoko*9/10)/2,y-(tate/2-2));
    line(x+(yoko*9/10)/2,y+(tate/2-1),x+(yoko*9/10)/2,y-(tate/2-2));
    line(x-(yoko/2-1),y-tate*4/10,x+(yoko/2-1),y-tate*4/10);
    line(x-(yoko*1.3/2),y+tate/2,x+(yoko*1.3/2),y+tate/2);
    Gara(pattern);
  }
  
 void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y-10),70,220);
         p1.b_display(2,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y-10),70,220);
         p1.s_display(2,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y-10),70,220);
         p1.f_display(2,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y-10),70,220);
         p1.h_display(2,1);
         break;
      default:
        break;
      }
   }
}
  
