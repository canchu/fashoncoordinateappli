//Written By Amai

class ColorPalette{
  /*variable definition*/
  int R,G,B;                 //color
  int x_gb,y_gb,x_r,y_r;       //color coordinates
  int x_yajirushi,y_yajirushi; //arrow coordinates
  
  ColorPalette(){
    /*color*/
    R=255;
    G=255;
    B=255;
    /*coordinates*/
    x_gb        = width*4/7;   //G,B x-coordinates
    y_gb        = height/3;    //G,B y-coordinates
    x_r         = x_gb+300;    //R   x-coordinates
    y_r         = y_gb;        //R   y-coordinates
    x_yajirushi = x_r+20;      //R arrowX x-coordinates
    y_yajirushi = y_r;         //R arrowY y-coordinates
  }
  
  /*display color*/
  void display_color(){
    rectMode(CORNERS);
    
    /*display G(topmost:255 bottommmost:0),B(leftmost:0 rightmmost:255)*/
    for(int i = y_gb; i < y_gb+256; i++){  //G:255~0
      for(int j = x_gb; j < x_gb+255; j++){  //B:0~255
        stroke(R,255-(i-y_gb),j-x_gb);
        fill(R,255-(i-y_gb),j-x_gb);
        point(j,i);
      }
    }
    
    /*display the arrow*/
    stroke(0);
    fill(255);
    triangle(x_yajirushi,y_yajirushi,x_yajirushi+20,y_yajirushi-10,x_yajirushi+20,y_yajirushi+10);

    /*display R(topmost:255 bottommmost:0)*/
    for(int i=y_r;i<y_r+256;i++){  //R:255~0
      for(int j=x_r;j<x_r+20;j++){
        stroke(255-(i-y_r),G,B);
        fill(255-(i-y_r),G,B);
        point(j,i);
      }
    }
  }
  
  /*display clothes*/
  void display_clothes(){   
    rectMode(CENTER);
    stroke(0);
    fill(R,G,B);
    rect(width/4,height/3,width/10,height/8);
  }
  
  /*if the mouse is pressed*/
  void mousePressed(){ 
    if((mouseX >= x_gb) && (mouseX < x_gb+256) && (mouseY >= y_gb) && (mouseY < y_gb+256)){
      B=mouseX-x_gb;
      G=255-(mouseY-y_gb);
    }
    else if((mouseX >= x_r) && (mouseX <x_yajirushi+20) && (mouseY >= y_r) && (mouseY < y_r+256)){
      R=255-(mouseY-y_r);
      y_yajirushi=mouseY;
    }
  }
  
}
