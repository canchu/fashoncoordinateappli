//Written by Amai

class Knit{
  float x;
  float y;
  float yoko;
  float tate;
  
  Knit(int hx,int hy){
    x    = hx*195/200;
    y    = hy*226/300;
    yoko = 90;
    tate = 110;
  }
  
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Knit*/ 
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.1/2),x-(yoko*3.8/10),y+(tate*1.2/2),x-(yoko*2.5/10),y+(tate/4));
    arc(x-(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-6.5),yoko/4.3,yoko/5,PI*1.5/4,PI*4.5/4);
    quad(x-((yoko*5.5/10)-1.5),y+((tate*1.1/2)+1),x-((yoko*5.5/10)+2.3),y+((tate*1.1/2)+10),x-((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x-((yoko*3.8/10)+1),y+(tate*1.2/2));

    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.1/2),x+(yoko*3.8/10),y+(tate*1.3/2),x+(yoko*2.5/10),y+(tate/4));
    arc(x+(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-4),yoko/4.3,yoko/5,-PI*0.5/4,PI*2.5/4);
    quad(x+((yoko*5.5/10)-1.5),y+((tate*1.1/2)+1),x+((yoko*5.5/10)+2.3),y+((tate*1.1/2)+10),x+((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x+((yoko*3.8/10)-0.5),y+(tate*1.2/2));
    
    //draw body
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/10),y-(tate/2));
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    arc(x-((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,HALF_PI,PI);
    arc(x+((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,0,HALF_PI);
    rect(x,y+(tate/2-tate/5+yoko/20),(yoko*3.3/5),yoko/10);
    line(x-((yoko*3/5)*1.1/2),y+(tate/2-tate/5+yoko/20),x+((yoko*3/5)*1.1/2),y+(tate/2-tate/5+yoko/20));
   
   
    
    /*draw the neck*/
    arc(x,y-(tate/2),yoko*1.1/2,yoko*0.8/2,0,PI);
    float a=yoko*1.1/4, b=yoko*0.8/4;
    for(float i=x-a;i<=x+a;i+=5){
      float x1=x,y1=y-(tate/2);
      float x0=i-x;
      float y0=y1+b*sqrt((1-(x0*x0)/(a*a)));
      line(i,y0,x1,y1);
    }   
    
    
    /*draw the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*4/10,tate*2/10,0, PI);
    
    
    /*disappear the lines*/
    stroke(R,G,B);
    fill(R,G,B);
    //disappear lines of the shoulder
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear lines of the body
    rect(x,y+(tate/2-tate/5-yoko/20+yoko*1.5/40),(yoko*3.3/5),yoko/10);
   
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+6),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+9),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+7),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+9),y+(tate/4));
    
     Gara(pattern);
  }
  
  void eri_l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Parka*/ 
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.1/2),x-(yoko*3.8/10),y+(tate*1.2/2),x-(yoko*2.5/10),y+(tate/4));
    arc(x-(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-6.5),yoko/4.3,yoko/5,PI*1.5/4,PI*4.5/4);
    quad(x-((yoko*5.5/10)-1.5),y+((tate*1.1/2)+1),x-((yoko*5.5/10)+2.3),y+((tate*1.1/2)+10),x-((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x-((yoko*3.8/10)+1),y+(tate*1.2/2));

    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.1/2),x+(yoko*3.8/10),y+(tate*1.3/2),x+(yoko*2.5/10),y+(tate/4));
    arc(x+(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-4),yoko/4.3,yoko/5,-PI*0.5/4,PI*2.5/4);
    quad(x+((yoko*5.5/10)-1.5),y+((tate*1.1/2)+1),x+((yoko*5.5/10)+2.3),y+((tate*1.1/2)+10),x+((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x+((yoko*3.8/10)-0.5),y+(tate*1.2/2));
    
    //draw body
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/10),y-(tate/2));
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    arc(x-((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,HALF_PI,PI);
    arc(x+((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,0,HALF_PI);
    rect(x,y+(tate/2-tate/5+yoko/20),(yoko*3.3/5),yoko/10);
    line(x-((yoko*3/5)*1.1/2),y+(tate/2-tate/5+yoko/20),x+((yoko*3/5)*1.1/2),y+(tate/2-tate/5+yoko/20));
   
    
    /*disappear the lines*/
    stroke(R,G,B);
    //disappear lines of the shoulder
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear lines of the body
    rect(x,y+(tate/2-tate/5-yoko/20+yoko*1.5/40),(yoko*3.3/5),yoko/10);
   
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+6),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+9),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+7),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+9),y+(tate/4));
    
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    quad(x-(yoko*4/10)/2,y-(tate/2+tate*0.2/3),x-(yoko*4/10)/2,y-(tate/2),x+(yoko*4/10)/2,y-(tate/2),x+(yoko*4/10)/2,y-(tate/2+tate*0.2/3));
    arc(x,y-tate/2,yoko*5/10,tate*2/10,0, PI);
    arc(x-((yoko*4/10)/2-1.5),y-(tate/2),yoko/12,tate*0.4/3,PI,HALF_PI*3);
    arc(x+((yoko*4/10)/2-1.5),y-(tate/2),yoko/12,tate*0.4/3,-HALF_PI,0);
    float w_left=0.5;
    for(float i=x-((yoko*4/10)/2-yoko/18);i<=x;i+=yoko/18){
      print(i+"\n");
      arc(i,y-(tate/2),yoko/(12+w_left),tate*0.4/3,PI,HALF_PI*3);
      w_left+=2;
    }
    float w_right=0.5;
    for(float i=x+((yoko*4/10)/2-yoko/18);i>=x;i-=yoko/18){
      print(i+"\n");
      arc(i,y-(tate/2),yoko/(12+w_right),tate*0.4/3,-HALF_PI,0);
      w_right+=2;
    } 
    Gara(pattern);
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y-10),70,100);
         p1.b_display(1,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y-10),70,90);
         p1.s_display(1,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y-10),70,100);
         p1.f_display(1,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y-10),70,100);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
  
  
}
    
