class Sneaker{
  float x;
  float y;
  float yoko;
  float tate;
  
  Sneaker(int hx,int hy){
    x    = hx*196/200;
    y    = hy*285/300;
    yoko = 59.5;
    tate = 80;
  }
  
  void s_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
   
    /*draw sneaker */
    //draw left pant
    quad(x-yoko*1/3,y+tate*2.5,x-yoko*1.25/3,y+tate*2.75,x-yoko*2.2/20,y+tate*2.75,x-yoko*2.5/20,y+tate*2.5);
    quad(x-yoko*1.25/3,y+tate*2.75,x-yoko*1.2/3,y+tate*2.87,x-yoko*2.7/20,y+tate*2.87,x-yoko*2.2/20,y+tate*2.75);
    arc(x-((yoko*1.3/3+yoko/15)/2+2),y+tate*2.75,yoko*0.7/3,yoko/9,0,PI);
    arc(x-((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
    
    arc(x-(yoko*1/3-11),y+tate*2.65,8,15,HALF_PI*2/3,HALF_PI*3);
    arc(x-(yoko*1/3+2),y+tate*2.65,8,15,-HALF_PI*1.8/3,HALF_PI);
    
    //left lace
    arc(x-((yoko*1/3+yoko*2.5/20)/2+3),y+tate*2.55,6,3,-PI,-PI/3);
    arc(x-((yoko*1/3+yoko*2.5/20)/2+3),y+tate*2.55,6,3,-PI/3,PI);
    arc(x-((yoko*1/3+yoko*2.5/20)/2-3),y+tate*2.55,6,3,-PI*2/3,0);
    arc(x-((yoko*1/3+yoko*2.5/20)/2-3),y+tate*2.55,6,3,0,PI*2/3);
    for(float i=y+tate*2.6;i<y+tate*2.7;i+=tate*0.08){
      line(x-(yoko*1/3-8),i,x-(yoko*1/3),i+tate*0.05);
      line(x-(yoko*1/3-1),i,x-(yoko*1/3-7),i+tate*0.05);
    }
  
    //draw right pant
    quad(x+yoko*1.15/3,y+tate*2.5,x+yoko*1.25/3,y+tate*2.75,x+yoko*2.2/20,y+tate*2.75,x+yoko*2.6/20,y+tate*2.5);
    quad(x+yoko*1.25/3,y+tate*2.75,x+yoko*1.2/3,y+tate*2.87,x+yoko*2.7/20,y+tate*2.87,x+yoko*2.2/20,y+tate*2.75);
    arc(x+((yoko*1.3/3+yoko/15)/2+2),y+tate*2.75,yoko*0.7/3,yoko/9,0,PI);
    arc(x+((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
    
    arc(x+(yoko*1/3-12),y+tate*2.65,8,15,-HALF_PI*2/3,HALF_PI*3.5/3);
    arc(x+(yoko*1/3+3),y+tate*2.65,8,15,HALF_PI,PI*4/3);
    
    //right lace
    arc(x+((yoko*1/3+yoko*2.5/20)/2+4),y+tate*2.55,6,3,-PI,-PI/3);
    arc(x+((yoko*1/3+yoko*2.5/20)/2+4),y+tate*2.55,6,3,-PI/3,PI);
    arc(x+((yoko*1/3+yoko*2.5/20)/2-2),y+tate*2.55,6,3,-PI*4/3,0);
    arc(x+((yoko*1/3+yoko*2.5/20)/2-2),y+tate*2.55,6,3,0,PI*2/3);
    for(float i=y+tate*2.6;i<y+tate*2.7;i+=tate*0.08){
      line(x+(yoko*1/3-8),i,x+(yoko*1/3),i+tate*0.05);
      line(x+(yoko*1/3-1),i,x+(yoko*1/3-7),i+tate*0.05);
    }
    
  
  
    /*disappear the lines*/
    stroke(R,G,B);
    //right
    line(x+(yoko*1.25/3-1),y+tate*2.75,x+(yoko*2.2/20+1),y+tate*2.75);
    
    //left
    line(x-(yoko*1.25/3-1),y+tate*2.75,x-(yoko*2.2/20+1),y+tate*2.75);
   
    Gara(pattern); 
  }
 
  
    void Gara(int pattern)
  {
      Pattern p1;
      Pattern p2;
      
      switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)(x-15),(int)(y+207),15,25);
         p2 = new Pattern((int)(x+15),(int)(y+207),15,25);
         p1.b_display(4,1);
         p2.b_display(4,1);   
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)(x-15),(int)(y+210),15,25);
         p2 = new Pattern((int)(x+15),(int)(y+210),15,25);
         p1.s_display(4,1);
         p2.s_display(4,1); 
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)(x-15),(int)(y+215),15,15);
         p2 = new Pattern((int)(x+15),(int)(y+215),15,15);
         p1.f_display(4,1);
         p2.f_display(4,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)(x-15),(int)(y+205),15,15);
         p2 = new Pattern((int)(x+15),(int)(y+205),15,15);
         p1.h_display(4,1);
         p2.h_display(4,1);
         break;
      default:
        break;
      }
  }
  
  
}
    
