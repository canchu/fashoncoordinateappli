//Written by Amai

class T_shirt{
  float x;
  float y;
  float yoko;
  float tate;
  
  T_shirt(int hx,int hy){
    x    = hx*195/200;
    y    = hy*220/300;
    yoko = 90;
    tate = 100;
  }
  
  void s_display(int R,int G,int B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a T-shirt*/
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.5,y+tate/8,x-(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2.5,y+tate/8);
    arc(x,y+tate/2,(yoko*3/5),yoko/7,0,PI);
    quad(x-yoko*3/10,y-tate/2,x-yoko*4/10,y-tate*4.5/10,x-yoko/2,y-tate*3/10,x-yoko*3/10,y-tate*2/10);
    quad(x+yoko*3/10,y-tate/2,x+yoko*4/10,y-tate*4.5/10,x+yoko/2,y-tate*3/10,x+yoko*3/10,y-tate*2/10);

    /*draw the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*2.8/10,tate*2/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    

    /*disappear the lines*/
    stroke(R,G,B);
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
     
    Gara(pattern);
  }
  
  void l_display(int R,int G,int B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a T-shirt*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5/10),y+(tate*1.5/2),x-(yoko*3.8/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5/10),y+(tate*1.5/2),x+(yoko*3.8/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    
    //draw body
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.5,y+tate/8,x-(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2.5,y+tate/8);
    arc(x,y+tate/2,(yoko*3/5),yoko/7,0,PI);
    
    /*draw the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*2.8/10,tate*2/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
  
    /*disappear the lines*/
    stroke(R,G,B);
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
   
    //disappear arm lines
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.5),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+2),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.3),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+2),y+(tate/4));
    
    Gara(pattern);
  }
  
  
 void Gara(int pattern){
   Pattern p1;
    switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)y,93,140);
         p1.b_display(1,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y-4),55,95);
         p1.s_display(1,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)y,60,100);
         p1.f_display(1,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)y,50,100);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
  
}
    
