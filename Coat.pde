//Written by Amai

class Coat{
  float x;
  float y;
  float yoko;
  float tate;
  
  Coat(int hx,int hy){
    x    = hx*195/200;
    y    = hy*220/300;
    yoko = 90;
    tate = 100;
  }
 
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Coat*/ 
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.35/2),x-(yoko*3.5/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    quad(x-(yoko*4.9/10),y+(tate*0.95/2),x-(yoko*5.3/10),y+(tate*1.1/2),x-(yoko*3.5/10),y+(tate*1.35/2),x-(yoko*2.5/10),y+(tate*1.3/2));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.35/2),x+(yoko*3.5/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    quad(x+(yoko*4.9/10),y+(tate*0.95/2),x+(yoko*5.3/10),y+(tate*1.1/2),x+(yoko*3.5/10),y+(tate*1.35/2),x+(yoko*2.5/10),y+(tate*1.3/2));
  
    //draw body
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+(tate/8),x-((yoko*3/5)/1.8+2),y+tate/2,x+((yoko*3/5)/1.8+2),y+tate/2,x+(yoko*3/5)/2.3,y+tate/8);
    quad(x-((yoko*3/5)/1.8+2),y+tate/2,x-(yoko*3/5)*2.2/3,y+tate*1.2,x+(yoko*3/5)*2.2/3,y+tate*1.2,x+((yoko*3/5)/1.8+2),y+tate/2);
    arc(x,y+tate*1.2,(yoko*3/5)*1.48,yoko/7,0,PI);
   
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8+1),y+tate/2,x+((yoko*3/5)/1.8+1),y+tate/2);
    
       
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.8),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+3.6),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.8),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+3.6),y+(tate/4));
    
    //disappear arm lines2
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    
 
    /*draw laces*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko/10,y-tate*2.5/8,yoko/8,tate/5,HALF_PI*1.7,0);
    
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    quad(x-(yoko*4/10)/2,y-(tate/2+tate*0.2/3),x-(yoko*4/10)/2,y-(tate/2),x+(yoko*4/10)/2,y-(tate/2),x+(yoko*4/10)/2,y-(tate/2+tate*0.2/3));
    
    //left
    quad(x-((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x-yoko*1.8/7,y-tate*0.8/2,x-yoko/12,y-tate*2/5,x-yoko/8,y-(tate/2+3));
    arc(x-yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI,-PI*1.65/4);
    //right
    quad(x+((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x+yoko*1.8/7,y-tate*0.8/2,x+yoko/12,y-tate*2/5,x+yoko/8,y-(tate/2+3));
    arc(x+yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI*2.35/4,0);
    
    //neck
    arc(x,y-tate/2+6,yoko*5.5/10,tate*2/10,0, PI);
    
    
    /*disappear the collar*/
    stroke(R,G,B);
    rect(x,y-tate/2+2,yoko*5/10,tate*1.5/10);
    
    /*draw the buttons*/
    stroke(0);
    fill(R,G,B);
    line(x+2,y-tate/3,x+2,y+tate*1.25);
    for(float i=y-(tate/3-5);i<y+tate;i+=30){
      strokeWeight(2);
      line(x-10,i,x+8,i);
      strokeWeight(1);
      fill(0);
      triangle(x,i-2,x-3,i+3,x+2,i);
      ellipse(x-10,i,3.5,3.5);
      ellipse(x+8,i,3.5,3.5);
    }
    Gara(pattern);
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.b_display(3,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+35),70,170);
         p1.s_display(3,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.f_display(3,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.h_display(3,1);
         break;
      default:
        break;
      }
   }
  
}
    
