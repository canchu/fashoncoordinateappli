//Written by Amai

class Poncho{
  float x;
  float y;
  float yoko;
  float tate;
  
  Poncho(int hx,int hy){
    x    = hx*195/200;
    y    = hy*226/300;
    yoko = 90;
    tate = 110;
  }
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Poncho*/ 
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*7/10),y+(tate*1.8/5),x-(yoko*2.5/10),y+(tate*2/5));
      
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.35/10),x+(yoko*7/10),y+(tate*1.8/5),x+(yoko*2.5/10),y+(tate*2/5));
   
    //draw body
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-((yoko*3/5)/2+yoko/20),y+(tate/2-tate/7),x+((yoko*3/5)/2+yoko/20),y+(tate/2-tate/7),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-((yoko*3/5)/2+2),y+((tate/2-tate/7)-(yoko/10)/2)+5,x-((yoko*3/5)/2+1),y+((tate/2-tate/7)+(yoko/10)/2),x+((yoko*3/5)/2+1),y+((tate/2-tate/7)+(yoko/10)/2),x+((yoko*3/5)/2+1),y+((tate/2-tate/7)-(yoko/10)/2)+5);
    arc(x-((yoko*3/5)/2),y+(tate/2-tate/7-2),yoko/10,yoko*1.3/10,HALF_PI,PI);
    arc(x+((yoko*3/5)/2),y+(tate/2-tate/7-2),yoko/10,yoko*1.3/10,0,HALF_PI);
 
 
    /*disappear the lines*/
    stroke(R,G,B);
    //disappear lines of the shoulder
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-6));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-5));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear lines of the parka
    line(x-((yoko*3/5)/2+2),y+((tate/2-tate/7)-(yoko/10)/2)+5,x+((yoko*3/5)/2+1),y+((tate/2-tate/7)-(yoko/10)/2)+5);
    rect(x,y+(tate/2-tate/5-yoko/20+yoko*1.5/40),(yoko*3.2/5),yoko/10);
   
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+6),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+9),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+7),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+9),y+(tate/4));
    
    
    /*draw laces*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko/10,y-tate*2.5/8,yoko/8,tate/5,HALF_PI*1.7,0);
    
     
    /*disappear the neck*/
    fill(254,237,217);
    rect(x,y-((tate/2)-(tate/10)/2),yoko/3,(tate/10));
    stroke(254,237,217);
    line(x-(yoko/3)/2,y-((tate/2)-0.5),x+(yoko/3)/2,y-((tate/2)-0.5));
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
    /*draw the buttons*/
    stroke(0);
    fill(R,G,B);
    line(x+9,y-tate*2/5,x+9.5,y+((tate/2-tate/7-2)+(yoko*1.3/20)));
    for(float i=y-(tate*2/5-5);i<y+10/*+((tate/2-tate/7-2)+(yoko*1.3/20)*/;i+=20){
      ellipse(x-5,i,3.5,3.5);
      ellipse(x+4,i,3.5,3.5);
    }
    
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    //left
    quad(x-((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x-yoko*1.8/7,y-tate*0.8/2,x-yoko/12,y-tate*2/5,x-yoko/8,y-(tate/2+3));
    arc(x-yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI,-PI*1.65/4);
    //right
    quad(x+((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x+yoko*1.8/7,y-tate*0.8/2,x+yoko/12,y-tate*2/5,x+yoko/8,y-(tate/2+3));
    arc(x+yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI*2.35/4,0);
    
    Gara(pattern);
  }
  
     
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y-12),125,99);
         p1.b_display(5,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y-10),90,90);
         p1.s_display(1,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y-10),90,100);
         p1.f_display(1,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y-10),80,95);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
  
}
    
