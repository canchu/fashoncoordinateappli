//Written by Yoshiko

class Pattern{
  PImage img,img2;
  int x;
  int y;
  int w;
  int h;
  //float R,G,B;
  Pattern(int fx,int fy,int fw,int fh){
    x=fx;
    y=fy+10;
    w=fw;
    h=fh;
  }
  void b_display(int t,int take){
    imageMode(CORNER);
     switch (t){    
     case 1:    //ue
        if(take==2){
          img = loadImage("s_boder1.png");
        }else{
          img = loadImage("s_border3.png");
        }
        break;

      case 2:    //shita
        if(take==1){  //take long
          img = loadImage("l_boder.png");
        }else{    //take short
          img =loadImage("s_boder1.png");
        }
        break;
      case 3:
        img = loadImage("lw_boder.png");
        break;
      case 4:    //komono
        if(take==1){
          img = loadImage("op_boder.png");
        }else{
          img = loadImage("komono_boder.png");
        }
        break;
      case 5://Poncho
        img = loadImage("a_boder.png");
      break;
      case 6://ShortSkirt
        img = loadImage("sk_boder.png");
       break;
      case 7: //Jacket
        img = loadImage("ta1_boder.png");
      break;
      case 8: //Boots Socks  /*if add*/
        img = loadImage("ti1_boder.png");
      default:
        break;
    }

    image(img,x-w/2,y-h/2,w,h);

  }
  void s_display(int t,int take){
     imageMode(CORNER);
     switch (t){    
      case 1:    //ue
        img = loadImage("s_stripe.png");
        break;
      case 2:    //shita
        if(take==1){  //take long
          img = loadImage("l_stripe.png");
        }else{    //take short
          img =loadImage("s_stripe.png");
        }
        break;
      case 3://Onepiece
        img = loadImage("l_stripe.png");
        break;
      case 4:    //komono
        if(take ==1){
          img = loadImage("opl_stripe.png");
          img2 = loadImage("opr_stripe.png");
      }else{
          img = loadImage("komono_stripe.png");
      }
        break;
      case 5: //Boots Socks
        img = loadImage("lt_stripe.png");
      break;
      case 6://Tights Socks KneeHigh
        img = loadImage("tai_stripe.png");
      break;
      case 7://Jacket
        img = loadImage("ta1_stripe.png");
      break;
      case 8: //Knit_Cap
        img = loadImage("knit_stripe.png");
      break;
      default:
        break;
    }

    image(img,x-w/2,y-h/2,w,h);

  }

  void f_display(int t,int take){
     imageMode(CORNER);
    stroke(0);
    switch (t){    
      case 1:    //ue
        img = loadImage("topsflower1.png");
        break;
      case 2:    //shita
        if(take==1){    //take long
          img = loadImage("ls_flower2.png");
        }else{    //take short
          img =loadImage("shortflower.png");
        }
        break;
      case 3:
        img = loadImage("ls_flower3.png");
        break;
      case 4:    //komono
        img = loadImage("hp1.png");
       break;
       case 5: //Jacket
        img = loadImage("ta1_flower.png");
       break;
       case 6://Socks,Boots
       if(take==1){
        img = loadImage("ti1_flower.png");
       }else{
         img = loadImage("socks_flower.png");
       }
        break;
       case 7: //Knit_Cap
        img = loadImage("knit_flower.png");
       break;
      default:
        break;
    }
      image(img,x-w/2,y-h/2,w,h);
  }
  
   void h_display(int t,int take){
      imageMode(CORNER);
      switch (t){    
      case 1:    //ue
        img = loadImage("short_Heart.png");
        break;
      case 2:    //shita
        if(take==1){  //take long
          img = loadImage("Heart2.png");
        }else{    //take short
          img =loadImage("short_Heart.png");
        }
        break;
      case 3:
        img = loadImage("Heart2.png");
        break;
      case 4:    //komono
        img = loadImage("sm1_Heart.png");
        break;
      case 5://Socks,Boots
        if(take==1){
        img = loadImage("ti1_Heart.png");
        }else{
          img = loadImage("socks_Heart.png");
        }
        break;
      case 6://Jacket
        img = loadImage("ta1_Heart.png");
        break;
      case 7: //Knit_Cap
        img = loadImage("knit_heart.png");
        break;
      default:
        break;
    }

    image(img,x-w/2,y-h/2,w,h);

  }
}

