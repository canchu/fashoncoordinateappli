//Written by Amai

class Cardigan{
  float x;
  float y;
  float yoko;
  float tate;
  
  Cardigan(int hx,int hy){
    x    = hx*195/200;
    y    = hy*226/300;
    yoko = 90;
    tate = 110;
  }
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Cardigan*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.1/2),x-(yoko*3.8/10),y+(tate*1.3/2),x-(yoko*2.5/10),y+(tate/4));
    arc(x-(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-4),yoko/4.3,yoko/5,PI*1.5/4,PI*4.5/4);
    quad(x-((yoko*5.5/10)-1),y+((tate*1.1/2)+5),x-((yoko*5.5/10)+2),y+((tate*1.1/2)+10),x-((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x-((yoko*3.8/10)+1),y+(tate*1.3/2));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.1/2),x+(yoko*3.8/10),y+(tate*1.3/2),x+(yoko*2.5/10),y+(tate/4));
    arc(x+(((yoko*5.5/10)+(yoko*3.8/10))/2-3),y+(((tate*1.1/2)+(tate*1.3/2))/2-4),yoko/4.3,yoko/5,-PI*0.5/4,PI*2.5/4);
    quad(x+((yoko*5.5/10)-1),y+((tate*1.1/2)+5),x+((yoko*5.5/10)+2),y+((tate*1.1/2)+10),x+((yoko*3.8/10)+3),y+((tate*1.1/2)+15),x+((yoko*3.8/10)+1),y+(tate*1.3/2));
    
    
    //draw body
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-3,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*0.5/3),y-(tate/2));
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+3,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*0.5/3),y-(tate/2));
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+((yoko*3/5)/2+yoko/20),y+(tate/2-tate/5),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    arc(x-((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,HALF_PI,PI);
    arc(x+((yoko*3/5)/2),y+(tate/2-tate/5),yoko/10,yoko/10,0,HALF_PI);
    rect(x,y+(tate/2-tate/5+yoko/20),(yoko*3.3/5),yoko/10);
    line(x-((yoko*3/5)*1.1/2),y+(tate/2-tate/5+yoko/20),x+((yoko*3/5)*1.1/2),y+(tate/2-tate/5+yoko/20));
    
    
    
    /*disappear the lines*/
    stroke(R,G,B);
    //disappear lines of the shoulder
    line(x-yoko*3/10,y-(tate*2/10+5)-5,x-yoko*3/10,y-(tate/2-6)-5);
    line(x+yoko*3/10,y-(tate*2/10+5)-5,x+yoko*3/10,y-(tate/2-5)-4);
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear lines of the cardigan
    rect(x,y+(tate/2-tate/5-yoko/20+yoko*1.5/40),(yoko*3.3/5),yoko/10);
   
    //disappear arm lines
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+6),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+9),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+7),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+9),y+(tate/4));
    
    
    /*draw cardigan lines*/
    stroke(0);
    fill(R,G,B);
    //median line
    line(x+3,y-(yoko*3.3/10)+(tate/2.5)/2,x+3,y+(tate/2-tate/5+yoko/20)+(yoko/20));
    for(float i=y-(yoko*3.3/10)+(tate/2.5)/2;i<y+(tate/2-tate/5+yoko/20)+(yoko/20);i+=20){
      ellipse(x,i,3,3);
    }
    //lines
    line(x-(yoko*3/10)*0.85,y-(tate/2),x-4,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-4,y-(yoko*3.3/10)+(tate/2.5)/2,x-4,y+(tate/2-tate/5+yoko/20)+(yoko/20));
    line(x+(yoko*3/10)*0.85,y-(tate/2),x+3.5,y-(yoko*3.3/10)+(tate/2.5)/2+1);
   
   Gara(pattern);
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)y,55,50);
         p1.b_display(1,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)y,70,70);
         p1.s_display(1,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)y,70,50);
         p1.f_display(1,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)y,70,50);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
}
    
