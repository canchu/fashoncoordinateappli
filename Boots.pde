//Written by Amai

class Boots{
  float x;
  float y;
  float yoko;
  float tate;
  
  Boots(int hx,int hy){
    x    = hx*196/200;
    y    = hy*285/300;
    yoko = 59.5;
    tate = 80;
  }
  
  void s_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
   
    /*draw socks */
    //draw left pant
    quad(x-yoko*1/3,y+tate*2.38,x-yoko*0.9/3,y+tate*2.5,x-yoko*2.7/20,y+tate*2.5,x-yoko*3/20,y+tate*2.38);
    quad(x-yoko*0.9/3,y+tate*2.5,x-yoko*1.25/3,y+tate*2.75,x-yoko*2.2/20,y+tate*2.75,x-yoko*2.7/20,y+tate*2.5);
    quad(x-yoko*1.25/3,y+tate*2.75,x-yoko*1.2/3,y+tate*2.87,x-yoko*2.7/20,y+tate*2.87,x-yoko*2.2/20,y+tate*2.75);
    arc(x-((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
  
    //draw right pant
    quad(x+yoko*1.1/3,y+tate*2.38,x+yoko*1.05/3,y+tate*2.5,x+yoko*2.7/20,y+tate*2.5,x+yoko*3/20,y+tate*2.38);
    quad(x+yoko*1.05/3,y+tate*2.5,x+yoko*1.25/3,y+tate*2.75,x+yoko*2.2/20,y+tate*2.75,x+yoko*2.7/20,y+tate*2.5);
    quad(x+yoko*1.25/3,y+tate*2.75,x+yoko*1.2/3,y+tate*2.87,x+yoko*2.7/20,y+tate*2.87,x+yoko*2.2/20,y+tate*2.75);
    arc(x+((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
  
  
    /*disappear the lines*/
    stroke(R,G,B);
    //right
    line(x+(yoko*1.05/3-1),y+tate*2.5,x+(yoko*2.7/20+1),y+tate*2.5);
    line(x+(yoko*1.25/3-1),y+tate*2.75,x+(yoko*2.2/20+1),y+tate*2.75);
    
    //left
    line(x-(yoko*0.9/3-1),y+tate*2.5,x-(yoko*2.7/20+1),y+tate*2.5);
    line(x-(yoko*1.25/3-1),y+tate*2.75,x-(yoko*2.2/20+1),y+tate*2.75);
    
    s_Gara(pattern);
  }
  
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
   
    /*draw socks */
    //draw left pant
    quad(x-yoko*1.4/3,y+tate*4.1/3,x-yoko*1.1/3,y+tate*2,x-yoko*3/20,y+tate*2,x-yoko*1.3/20,y+tate*4.15/3);
    quad(x-yoko*1.1/3,y+tate*2,x-yoko*0.9/3,y+tate*2.5,x-yoko*2.7/20,y+tate*2.5,x-yoko*3/20,y+tate*2);
    quad(x-yoko*0.9/3,y+tate*2.5,x-yoko*1.25/3,y+tate*2.75,x-yoko*2.2/20,y+tate*2.75,x-yoko*2.7/20,y+tate*2.5);
    quad(x-yoko*1.25/3,y+tate*2.75,x-yoko*1.2/3,y+tate*2.87,x-yoko*2.7/20,y+tate*2.87,x-yoko*2.2/20,y+tate*2.75);
    arc(x-((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
  
    //draw right pant
    quad(x+yoko*1.3/3,y+tate*4.1/3,x+yoko*1.1/3,y+tate*2,x+yoko*3/20,y+tate*2,x+yoko*1.3/20,y+tate*4.15/3);
    quad(x+yoko*1.1/3,y+tate*2,x+yoko*1.05/3,y+tate*2.5,x+yoko*2.7/20,y+tate*2.5,x+yoko*3/20,y+tate*2);
    quad(x+yoko*1.05/3,y+tate*2.5,x+yoko*1.25/3,y+tate*2.75,x+yoko*2.2/20,y+tate*2.75,x+yoko*2.7/20,y+tate*2.5);
    quad(x+yoko*1.25/3,y+tate*2.75,x+yoko*1.2/3,y+tate*2.87,x+yoko*2.7/20,y+tate*2.87,x+yoko*2.2/20,y+tate*2.75);
    arc(x+((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
  
  
    /*disappear the lines*/
    stroke(R,G,B);
    //right
    line(x+(yoko*1.1/3-1),y+tate*2,x+(yoko*3/20+1),y+tate*2);
    line(x+(yoko*1.05/3-1),y+tate*2.5,x+(yoko*2.7/20+1),y+tate*2.5);
    line(x+(yoko*1.25/3-1),y+tate*2.75,x+(yoko*2.2/20+1),y+tate*2.75);
    
    //left
    line(x-(yoko*1.1/3-1),y+tate*2,x-(yoko*3/20+1),y+tate*2);
    line(x-(yoko*0.9/3-1),y+tate*2.5,x-(yoko*2.7/20+1),y+tate*2.5);
    line(x-(yoko*1.25/3-1),y+tate*2.75,x-(yoko*2.2/20+1),y+tate*2.75);
    
     l_Gara(pattern);
  }
    
   
  void s_Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+200),95,50);
         p1.b_display(8,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+205),50,50);
         p1.s_display(6,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+200),40,50);
         p1.f_display(6,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+200),40,50);
         p1.h_display(5,2);
         break;
      default:
        break;
      }
   }
  
  void l_Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+155),100,120);
         p1.b_display(8,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+160),45,120);
         p1.s_display(2,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+155),50,120);
         p1.f_display(6,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+155),50,120);
         p1.h_display(5,1);
         break;
      default:
        break;
      }
  }
}
    
