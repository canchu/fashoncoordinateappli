//Written by Amai

class Kome{
  int komeX;
  int komeY;
  
  Kome(int x,int y){
    komeX=x;
    komeY=y;
  }
  
  void display(){
    /*kome*/
    ellipseMode(CENTER);
    rectMode(CORNER);
    
    //body-out
    stroke(0);
    fill(255);
    ellipse(komeX,komeY,52,77);
    ellipse(komeX,komeY+13,52,52);
    line(komeX-25,komeY+5,komeX-25,komeY+10);
    line(komeX+26,komeY+5,komeX+25,komeY+10);
  
    //body-in
    stroke(255);
    fill(255);
    ellipse(komeX,komeY,50,75);
    ellipse(komeX,komeY+13,50,50);
    line(komeX-24,komeY+5,komeX-24,komeY+10);
    line(komeX+25,komeY+5,komeX+24,komeY+10);
    
    //eye
    stroke(0);
    fill(0);
    ellipse(komeX-13,komeY-25,1,1);
    ellipse(komeX-1,komeY-25,1,1);
    
    //mouth
    fill(255);
    rect(komeX-12,komeY-22,10,2);
    
    //character
    line(komeX-15,komeY+5,komeX,komeY+5);
    line(komeX-15,komeY,komeX-10,komeY+5);
    line(komeX-15,komeY+10,komeX-10,komeY+5);
    line(komeX-10,komeY-1,komeX-10,komeY+12);
    line(komeX-10,komeY+5,komeX-2,komeY);
    line(komeX-10,komeY+5,komeX-3,komeY+10);
    line(komeX-3,komeY+10,komeX-1,komeY+10);
  }
}
