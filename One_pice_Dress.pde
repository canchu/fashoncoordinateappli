//Written by Amai
class One_piece_Dress{
  float x;
  float y;
  float yoko;
  float tate;
  
  One_piece_Dress(int hx,int hy){
    x    = hx*195/200;
    y    = hy*220/300;
    yoko = 90;
    tate = 100;
    //the hight is y+tate/2.  
  }
  
  void s_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw a dress
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+tate/8,x-(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/2.3,y+tate/8);
    quad(x-(yoko*3/5)/1.8,y+tate/2,x-(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)/1.8,y+tate/2);
    arc(x,y+tate*1.3,(yoko*3/5)*1.33,yoko/7,0,PI);
    
    //draw arms
    quad(x-yoko*3/10,y-tate/2,x-yoko*4/10,y-tate*4.5/10,x-yoko/2,y-tate*3/10,x-yoko*3/10,y-tate*2/10);
    quad(x+yoko*3/10,y-tate/2,x+yoko*4/10,y-tate*4.5/10,x+yoko/2,y-tate*3/10,x+yoko*3/10,y-tate*2/10);
    
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8-1),y+tate/2,x+((yoko*3/5)/1.8-1),y+tate/2);
    line(x-((yoko*3/5)*2/3-1),y+tate*1.3,x+((yoko*3/5)*2/3-1),y+tate*1.3);
    
    //disappear arm lines
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
     
    /*disappear the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*2.8/10,tate*2/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
    s_Gara(pattern);
  }
  
  void eri_s_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw a dress
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+tate/8,x-(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/2.3,y+tate/8);
    quad(x-(yoko*3/5)/1.8,y+tate/2,x-(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)/1.8,y+tate/2);
    arc(x,y+tate*1.3,(yoko*3/5)*1.33,yoko/7,0,PI);
    
    //draw arms
    quad(x-yoko*3/10,y-tate/2,x-yoko*4/10,y-tate*4.5/10,x-yoko/2,y-tate*3/10,x-yoko*3/10,y-tate*2/10);
    quad(x+yoko*3/10,y-tate/2,x+yoko*4/10,y-tate*4.5/10,x+yoko/2,y-tate*3/10,x+yoko*3/10,y-tate*2/10);
    
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8-1),y+tate/2,x+((yoko*3/5)/1.8-1),y+tate/2);
    line(x-((yoko*3/5)*2/3-1),y+tate*1.3,x+((yoko*3/5)*2/3-1),y+tate*1.3);
    
    //disappear arm lines
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko*1/10,y-tate*1.5/3,yoko/3.5,yoko/3,HALF_PI/2,PI);
    arc(x+yoko*1/10,y-tate*1.5/3,yoko/3.5,yoko/3,0,HALF_PI*3/2);
    arc(x-yoko*1.3/10,y-tate*1.5/3,yoko/5,yoko/8,-PI,-PI*1.3/4);
    arc(x+yoko*1.3/10,y-tate*1.5/3,yoko/5,yoko/8,-PI*2.7/4,0);
    
    /*disappear the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*2.8/10,tate*2/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
     s_Gara(pattern);
  }
  
  void l_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5/10),y+(tate*1.5/2),x-(yoko*3.8/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5/10),y+(tate*1.5/2),x+(yoko*3.8/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    
    //draw a dress
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+tate/8,x-(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/2.3,y+tate/8);
    quad(x-(yoko*3/5)/1.8,y+tate/2,x-(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)/1.8,y+tate/2);
    arc(x,y+tate*1.3,(yoko*3/5)*1.33,yoko/7,0,PI);
    
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8-1),y+tate/2,x+((yoko*3/5)/1.8-1),y+tate/2);
    line(x-((yoko*3/5)*2/3-1),y+tate*1.3,x+((yoko*3/5)*2/3-1),y+tate*1.3);
    
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.8),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+3.6),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.8),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+3.6),y+(tate/4));
    
    //disappear arm lines2
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
     
    /*disappear the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*2.8/10,tate*2/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
     l_Gara(pattern);
  }
  
  void eri_l_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5/10),y+(tate*1.5/2),x-(yoko*3.8/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5/10),y+(tate*1.5/2),x+(yoko*3.8/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    
    //draw a dress
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+tate/8,x-(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/1.8,y+tate/2,x+(yoko*3/5)/2.3,y+tate/8);
    quad(x-(yoko*3/5)/1.8,y+tate/2,x-(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)*2/3,y+tate*1.3,x+(yoko*3/5)/1.8,y+tate/2);
    arc(x,y+tate*1.3,(yoko*3/5)*1.33,yoko/7,0,PI);
    
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8-1),y+tate/2,x+((yoko*3/5)/1.8-1),y+tate/2);
    line(x-((yoko*3/5)*2/3-1),y+tate*1.3,x+((yoko*3/5)*2/3-1),y+tate*1.3);
    
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.8),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+3.6),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.8),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+3.6),y+(tate/4));
    
    //disappear arm lines2
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    arc(x-yoko*1/10,y-tate*1.5/3,yoko/3.5,yoko/3,HALF_PI/2,PI);
    arc(x+yoko*1/10,y-tate*1.5/3,yoko/3.5,yoko/3,0,HALF_PI*3/2);
    arc(x-yoko*1.3/10,y-tate*1.5/3,yoko/5,yoko/8,-PI,-PI*1.3/4);
    arc(x+yoko*1.3/10,y-tate*1.5/3,yoko/5,yoko/8,-PI*2.7/4,0);
    
    /*disappear the collar*/
    fill(254,237,217);
    arc(x,y-tate/2,yoko*2.8/10,tate*2/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
    l_Gara(pattern);
  }
  
  void l_Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.b_display(3,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+35),70,170);
         p1.s_display(3,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.f_display(3,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.h_display(3,1);
         break;
      default:
        break;
      }
   }
  void s_Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+30),60,180);
         p1.b_display(3,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+35),60,170);
         p1.s_display(3,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+30),60,180);
         p1.f_display(3,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+30),60,180);
         p1.h_display(3,1);
         break;
      default:
        break;
      }
   }
  
}
    
