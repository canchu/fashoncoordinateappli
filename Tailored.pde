//Written by Amai

class Tailored{
  float x;
  float y;
  float yoko;
  float tate;
  
  Tailored(int hx,int hy){
    x    = hx*195/200;
    y    = hy*226/300;
    yoko = 90;
    tate = 110;
  }
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Cardigan*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.35/2),x-(yoko*3.5/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.35/2),x+(yoko*3.5/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
   
    //right body
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+3,y-(yoko*3.3/10)+(tate/2.5)/2,x+yoko/5,y-(tate/2)); 
    quad(x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2.3,y+tate/8,x-3,y+tate/8,x+3,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x+(yoko*3/5)/2.3,y+tate/8,x+((yoko*3/5)/1.8+2),y+tate/2,x+8,y+tate/2,x-3,y+tate/8);
    arc(x+(((yoko*3/5)/1.8+2)+8)/2,y+tate/2,yoko/4,yoko/8,0,PI);
    
    //left body
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-3,y-(yoko*3.3/10)+(tate/2.5)/2,x-yoko/5,y-(tate/2));  
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+3,y+tate/8,x-3,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+tate/8,x-((yoko*3/5)/1.8+2),y+tate/2,x-8,y+tate/2,x+3,y+tate/8);
    arc(x-(((yoko*3/5)/1.8+2)+8)/2,y+tate/2,yoko/4,yoko/8,0,PI);

    /*disappear the lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x-10,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x+10,y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    //line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    //line(x-((yoko*3/5)/1.8+1),y+tate/2,x+((yoko*3/5)/1.8+1),y+tate/2);
       
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.8),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+3.6),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.8),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+5),y+(tate/4));
    
    //disappear arm lines2
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    //right
    quad(x+((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x+yoko*2.2/7,y-tate*0.8/2,x+yoko/6,y-tate*2/5,x+yoko/8,y-(tate/2+3));
    arc(x+yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI*2.35/4,0);
    quad(x+yoko/6,y-tate*2/5,x+yoko/3.5,y-tate/3.5,x-0.5,y+tate/8-5,x-2,y+tate/8-8);
   
   
    //left
    quad(x-((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x-yoko*2.2/7,y-tate*0.8/2,x-yoko/6,y-tate*2/5,x-yoko/8,y-(tate/2+3));
    arc(x-yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI,-PI*1.65/4);
    quad(x-yoko/6,y-tate*2/5,x-yoko/3.5,y-tate/3.5,x+2,y+tate/8,x+3,y+tate/8);
    
    
    
    /*draw the button*/
    stroke(0);
    fill(R,G,B);
    ellipse(x,y+tate/8,3.5,3.5);
    
    Gara(pattern);
  }
  
   void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)y,75,130);
         p1.b_display(7,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+5),105,130);
         p1.s_display(7,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)(x+5),(int)(y-10),70,130);
         p1.f_display(5,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x+4,(int)(y-14),80,100);
         p1.h_display(6,1);
         break;
      default:
        break;
      }
   }
}
    
