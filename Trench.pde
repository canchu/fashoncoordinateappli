//Written by Amai

class Trench{
  float x;
  float y;
  float yoko;
  float tate;;
  
  Trench(int hx,int hy){
    x    = hx*195/200;
    y    = hy*220/300;
    yoko = 90;
    tate = 100;
    //the hight is y+tate/2.  
  }
  
  void l_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.35/2),x-(yoko*3.5/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    quad(x-(yoko*4.9/10),y+(tate*0.95/2),x-(yoko*5.3/10),y+(tate*1.1/2),x-(yoko*3.5/10),y+(tate*1.35/2),x-(yoko*2.5/10),y+(tate*1.3/2));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.35/2),x+(yoko*3.5/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    quad(x+(yoko*4.9/10),y+(tate*0.95/2),x+(yoko*5.3/10),y+(tate*1.1/2),x+(yoko*3.5/10),y+(tate*1.35/2),x+(yoko*2.5/10),y+(tate*1.3/2));
  
    //draw the chest
    quad(x+(yoko*3/5)/2,y-((yoko*3.3/10)+(tate/2.5)/2),x+(yoko*3/5)/2,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/5,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/4,y-((yoko*3.3/10)+(tate/2.5)/2));
    quad(x-(yoko*3/5)/2,y-((yoko*3.3/10)+(tate/2.5)/2),x-((yoko*3/5)/2),y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/5,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/4,y-((yoko*3.3/10)+(tate/2.5)/2));
    
    //draw the body
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2.3,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.3,y+((tate/8)+3),x-((yoko*3/5)/1.8+2),y+tate/2,x+((yoko*3/5)/1.8+2),y+tate/2,x+(yoko*3/5)/2.3,y+tate/8);
    quad(x-((yoko*3/5)/1.8+2),y+tate/2,x-(yoko*3/5)*2.2/3,y+tate*1.2,x+(yoko*3/5)*2.2/3,y+tate*1.2,x+((yoko*3/5)/1.8+2),y+tate/2);
    arc(x,y+tate*1.2,(yoko*3/5)*1.48,yoko/7,0,PI);
   
   
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8+1),y+tate/2,x+((yoko*3/5)/1.8+1),y+tate/2);
       
    //disappear arm lines1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.8),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+3.6),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.8),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+3.6),y+(tate/4));
    
    //disappear arm lines2
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    
    /*draw the button*/
    stroke(0);
    fill(R,G,B);
    line(x+(yoko*3/5)/5,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/5,y+((tate*1.2)+(yoko/7)-6));
    for(float i=y-((yoko*3.3/10)-(tate/2.5)/2+4-10);i<y+((tate*0.5));i+=20){
      ellipse(x+((yoko*3/5)/5-5),i,5,5);
    }
    for(float i=y-((yoko*3.3/10)-(tate/2.5)/2+4+10);i<y+((tate*0.5));i+=20){
      ellipse(x-((yoko*3/5)/5+2),i,5,5);
    }
    
    /*draw the collar*/
    stroke(0);
    fill(R,G,B);
    //right
    quad(x+((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x+yoko*1.8/7,y-tate*0.8/2,x+yoko/12,y-tate*2/5,x+yoko/8,y-(tate/2+3));
    arc(x+yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI*2.35/4,0);
    quad(x+yoko/12,y-tate*2/5,x+yoko/4.5,y-tate/3.5,x-1,y-tate/5,x-2,y-tate/4.5);
   
    //left
    quad(x-((yoko*0.65/5)+(yoko/3)/2),y-(tate/2+0.2),x-yoko*1.8/7,y-tate*0.8/2,x-yoko/12,y-tate*2/5,x-yoko/8,y-(tate/2+3));
    arc(x-yoko*0.65/5,y-tate/2,yoko/3,tate/5,-PI,-PI*1.65/4);
    quad(x-yoko/12,y-tate*2/5,x-yoko/4.5,y-tate/3.5,x+(yoko*3/5)/5-1,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/5,y-((yoko*3.3/10)-(tate/2.5)/2));
    
   
    /*draw the belt*/
    quad(x-((yoko*3/5)/2.3+2),y+((tate/8)-3),x-((yoko*3/5)/2.3+2),y+((tate/8)+3),x+((yoko*3/5)/2.3+2),y+((tate/8)+3),x+((yoko*3/5)/2.3+2),y+((tate/8)-3));
    quad(x+((yoko*3/5)/2.3),y+((tate/8)),x+((yoko*3/5)/2.3-7),y+((tate/8)+5),x+((yoko*3/5)/2.3-15),y+((tate/2)),x+((yoko*3/5)/2.3-8),y+((tate*1.1/2)));
    quad(x+((yoko*3/5)/2.3),y+((tate/8)),x+((yoko*3/5)/2.3+5),y+((tate/8)+5),x+((yoko*3/5)/2.3+8),y+((tate*0.8)),x+((yoko*3/5)/2.3+3),y+((tate*0.85)));
    quad(x+((yoko*3/5)/2.3-3),y+((tate/8)-5),x+((yoko*3/5)/2.3-6),y+((tate/8)),x+((yoko*3/5)/2.3),y+((tate/8)+5),x+((yoko*3/5)/2.3+1),y+((tate/8)-1));
   
    Gara(pattern);   
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.b_display(3,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+35),70,140);
         p1.s_display(3,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.f_display(3,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+30),70,180);
         p1.h_display(3,1);
         break;
      default:
        break;
      }
   }
  
  
}
    
