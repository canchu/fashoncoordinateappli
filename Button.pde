//Written by Ayano

class Button{
  int     mX;
  int     mY;
  int     mHeight;
  int     mWidth;
  int     mSize;
  String  mText;
  color   mButtonColor;
  color   mButtonReverseColor;
  boolean mButtonPressed;
  PImage  mFlowerImage;
  
  Button(int aButtonX,int aButtonY,int aWidth,int aHeight,int aSize,String aText,color aColor,color aReverseColor){
    mX                  = aButtonX;
    mY                  = aButtonY;
    mHeight             = aHeight;
    mWidth              = aWidth;
    mSize               = aSize;
    mText               = aText;
    mButtonColor        = aColor;
    mButtonReverseColor = aReverseColor;
  }
  
  void Disp(){
    rectMode(CORNER);
    mFlowerImage = loadImage("Flower.png");
    Is_Button_Pressed();
    
    if(mouseX > mX  && mouseX < mX + mWidth ){
      if(mouseY > mY && mouseY <mY + mHeight){
        fill(mButtonReverseColor);
      }
      else{
         fill(mButtonColor);
      }
    }
    else{
       fill(mButtonColor);
    }
   
    textSize(mSize);
    text(mText,mX+25,mY,mWidth,mHeight);
    noFill();
    stroke(200,100,100);
    if(gPageNumber!=0){
      rect(mX+0.5*mSize,mY,mWidth,mHeight);
    }
    if(gPageNumber!=2 && gPageNumber!=0 && gPageNumber!=3){
      image(mFlowerImage,mX+10,mY);
      image(mFlowerImage,mX+mWidth+15,mY+mHeight);
    }
  }
  
  void Is_Button_Pressed(){
    if(mouseX > mX && mouseX < mX + mWidth){
      if(mouseY > mY && mouseY < mY + mHeight){
        if(mousePressed){
          sound = minim.loadSnippet("Button.mp3");
          sound.play();
          mButtonPressed = true;
        }
        else{
          mButtonPressed = false;
        }
      }
    }
    else{
      mButtonPressed = false;
    }
  }
  
}
