//Written by Amai
class Short_Skirt{
  float x;
  float y;
  float yoko;
  float tate;
  
  Short_Skirt(int hx,int hy){
    x    = hx*196/200;
    y    = hy*310/300;
    yoko = 60.5;
    tate = 80;
  }
  
  void s_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw short_skirt*/
    quad(x-(yoko*8.5/10)/2,y-(tate*1.1/2+tate/24),x-(yoko*9/10)/2,y-tate*1.1/2+tate/24,x+(yoko*9/10)/2,y-tate*1.1/2+tate/24,x+(yoko*8/10)/2,y-(tate*1.1/2+tate/24));
    rect(x,y,yoko*9/10,tate);
    triangle(x-(yoko*9/10)/2,y-tate/2,x-yoko/2,y-tate*3/10,x-(yoko*9/10)/2,y-tate*3/10);
    triangle(x+(yoko*9/10)/2,y-tate/2,x+yoko/2,y-tate*3/10,x+(yoko*9/10)/2,y-tate*3/10);
    quad(x-yoko/2,y-tate*3/10,x-yoko*4/6.5,y+tate/2,x-(yoko*9/10)/2,y+tate/2,x-(yoko*9/10)/2,y-tate/2);
    quad(x+yoko/2,y-tate*3/10,x+yoko*4/6.5,y+tate/2,x+(yoko*9/10)/2,y+tate/2,x+(yoko*9/10)/2,y-tate/2);
    arc(x,y+tate/2,(yoko*9/10+(yoko*4/6.5)*2)/2+5,yoko/7,0,PI);
  
    /*disappear the lines*/
    stroke(R,G,B);
    line(x-(yoko*9/10)/2,y+(tate/2-1),x-(yoko*9/10)/2,y-(tate/2-2));
    line(x+(yoko*9/10)/2,y+(tate/2-1),x+(yoko*9/10)/2,y-(tate/2-2));
    line(x-(yoko/2-1),y-tate*3/10,x+(yoko/2-1),y-tate*3/10);
    
    Gara(pattern);
    
  }
  
   void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y-10),80,85);
         p1.b_display(6,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y-10),60,85);
         p1.s_display(2,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y-10),60,80);
         p1.f_display(2,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y-10),60,80);
         p1.h_display(2,2);
         break;
      default:
        break;
      }
   }
}
    
