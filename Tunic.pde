//Written by Amai

class Tunic{
  float x;
  float y;
  float yoko;
  float tate;
  
  Tunic(int hx,int hy){
    x    = hx*195/200;
    y    = hy*220/300;
    yoko = 90;
    tate = 100;
    //the hight is y+tate*0.73.  
  }
  
  void s_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.5/10),y,x-(yoko*2.5/10),y+8);
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.5/10),y,x+(yoko*2.5/10),y+8);  
    
    //draw the chest
    quad(x-(yoko*3/5)/2,y-((yoko*3.3/10)+(tate/2.5)/2),x-yoko*3/10,y-tate*2/10,x+yoko*3/10,y-tate*2/10,x+(yoko*3/5)/2,y-((yoko*3.3/10)+(tate/2.5)/2));
    quad(x-yoko*3/10,y-tate*2/10,x-(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2),x+yoko*3/10,y-tate*2/10);
    triangle(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.8,y-(yoko*3.3/10)+(tate/2.5)/2.3,x,y-(yoko*3.3/10)+(tate/2.5)/2);
    triangle(x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2.8,y-(yoko*3.3/10)+(tate/2.5)/2.3,x,y-(yoko*3.3/10)+(tate/2.5)/2);
    
    //draw the body
    quad(x-(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)*2/3,y+tate*0.73,x+(yoko*3/5)*2/3,y+tate*0.73,x+(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2));
    arc(x,y+tate*0.73,(yoko*3/5)*1.33,yoko/7,0,PI);
  
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-(yoko*3/10-1),y-tate*2/10,x+(yoko*3/10-1),y-tate*2/10);
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8-1),y+tate/2,x+((yoko*3/5)/1.8-1),y+tate/2);
    
    //disappear arm lines
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    /*draw folds*/
    stroke(0);
    fill(R,G,B,100);
    line(x-(yoko*3/5)/3,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/2.5,y+(tate*1.5/3));
    line(x-(yoko*3/5)/4,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/3.8,y+(tate*1.8/3));
    line(x-(yoko*3/5)/9,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/12,y+(tate*1.5/3));
    line(x,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/19,y+(tate*1/3));
    line(x+(yoko*3/5)/3,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/2.5,y+(tate*1.5/3));
    line(x+(yoko*3/5)/4,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/3.8,y+(tate*2/3));
    line(x+(yoko*3/5)/9,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/9.5,y+(tate*0.8/3));
    
    /*disappear the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*4/10,tate*2.5/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    
    Gara(pattern);
  }

  
  void l_display(float R,float G,float B,int pattern){
    /*initialization Mode*/
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a one_pice_dress*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5.5/10),y+(tate*1.35/2),x-(yoko*3.5/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5.5/10),y+(tate*1.35/2),x+(yoko*3.5/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    
    //draw the chest
    quad(x-(yoko*3/5)/2,y-((yoko*3.3/10)+(tate/2.5)/2),x-yoko*3/10,y-tate*2/10,x+yoko*3/10,y-tate*2/10,x+(yoko*3/5)/2,y-((yoko*3.3/10)+(tate/2.5)/2));
    quad(x-yoko*3/10,y-tate*2/10,x-(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2),x+yoko*3/10,y-tate*2/10);
    triangle(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.8,y-(yoko*3.3/10)+(tate/2.5)/2.3,x,y-(yoko*3.3/10)+(tate/2.5)/2);
    triangle(x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2.8,y-(yoko*3.3/10)+(tate/2.5)/2.3,x,y-(yoko*3.3/10)+(tate/2.5)/2);
    
    //draw the body
    quad(x-(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)*2/3,y+tate*0.73,x+(yoko*3/5)*2/3,y+tate*0.73,x+(yoko*3/5)/1.9,y-((yoko*3.3/10)-(tate/2.5)/2));
    arc(x,y+tate*0.73,(yoko*3/5)*1.33,yoko/7,0,PI);
  
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear dress lines
    line(x-(yoko*3/10-1),y-tate*2/10,x+(yoko*3/10-1),y-tate*2/10);
    line(x-((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2,x+((yoko*3/5)/2-1),y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-((yoko*3/5)/2.3-1),y+tate/8,x+((yoko*3/5)/2.3-1),y+tate/8);
    line(x-((yoko*3/5)/1.8-1),y+tate/2,x+((yoko*3/5)/1.8-1),y+tate/2);
     
    //disappear arm lines 1
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+8),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+10),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+8.3),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+10.2),y+(tate/4));
    
    //disappear shoulder lines
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    
    
    /*draw folds*/
    stroke(0);
    fill(R,G,B,100);
    line(x-(yoko*3/5)/3,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/2.5,y+(tate*1.5/3));
    line(x-(yoko*3/5)/4,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/3.8,y+(tate*1.8/3));
    line(x-(yoko*3/5)/9,y-((yoko*3.3/10)-(tate/2.5)/2),x-(yoko*3/5)/12,y+(tate*1.5/3));
    line(x,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/19,y+(tate*1/3));
    line(x+(yoko*3/5)/3,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/2.5,y+(tate*1.5/3));
    line(x+(yoko*3/5)/4,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/3.8,y+(tate*2/3));
    line(x+(yoko*3/5)/9,y-((yoko*3.3/10)-(tate/2.5)/2),x+(yoko*3/5)/9.5,y+(tate*0.8/3));
    
    /*disappear the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*4/10,tate*2.5/10,0, PI);
    stroke(254,237,217);
    line(x-(yoko*2.8/20-1),y-tate/2-1,x+(yoko*2.8/20-1),y-tate/2-1);
    Gara(pattern);
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        break;
      case 2:
         p1 = new Pattern((int)x,(int)(y+5),65,120);
         p1.b_display(1,2);
        break;
      case 3:    
         p1 = new Pattern((int)x,(int)(y+7),65,120);
         p1.s_display(1,2);
        break;
       case 4:
         p1 = new Pattern((int)x,(int)y,60,120);
         p1.f_display(1,2);
         break;
       case 5:
         p1 = new Pattern((int)x,(int)y,50,120);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
  
}
    
