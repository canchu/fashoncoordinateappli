//Writen by Amai

class Knit_Cap{
  float x;
  float y;
  float yoko;
  float tate;
  
  Knit_Cap(int hx,int hy){
    x    = hx*193/200;
    y    = hy*120/300;
    yoko = 61;
    tate = 55;
  }
  
  void s_display(int R, int G, int B, int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    
    /*draw the cap*/
    arc(x, y, yoko, tate, -PI, 0);
    rect(x, y+(tate/5)/2, yoko*1.02, tate/5);
    arc(x-((yoko*1.02)/2-0.5), y+(tate/5)/2, 7, tate/5, HALF_PI, HALF_PI*3);
    arc(x+((yoko*1.02)/2-0.5), y+(tate/5)/2, 7, tate/5, -HALF_PI, HALF_PI);
    
    //draw folds
    float r_folds=6.5;
    //left
    for(float i=x-((yoko*1.02)/2+0.5); i<=x; i+=6){
      arc(i, y+(tate/5)/2, r_folds, tate/5, HALF_PI, HALF_PI*3);
      r_folds-=0.5;
    }
    r_folds=6.5;
    
    //right
    for(float i=x+((yoko*1.02)/2+0.5); i>=x; i-=6){
      arc(i, y+(tate/5)/2, r_folds, tate/5, -HALF_PI, HALF_PI);
      r_folds-=0.5;
    }
    
    /*draw bonbon*/
    //median bonbon
    float bonY=y-(tate/2+3);
    float bonX=x;
    float r=5.5;
    stroke(0);
    for(float i=-r;i<=r;i+=r/2){
      arc(bonX+i, bonY+sqrt(r*r-i*i), r, r, 0, PI*2);
      arc(bonX+i, bonY-sqrt(r*r-i*i), r, r, 0, PI*2);
    }
    noStroke();
    fill(R,G,B);
    arc(bonX, bonY, r*2, r*2, 0, PI*2);
   
    Gara(pattern);
  }
  
    
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)y-15,65,40);
         p1.b_display(6,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x+2,(int)y-22,100,100);
         p1.s_display(8,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)y-22,60,60);
         p1.f_display(7,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)y-22,60,60);
         p1.h_display(7,2);
         break;
      default:
        break;
    }
  }
}
