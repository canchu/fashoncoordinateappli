//Written by Amai

class Tights{
  float x;
  float y;
  float yoko;
  float tate;
  
  Tights(int hx,int hy){
    x    = hx*196/200;
    y    = hy*285/300;
    yoko = 59.5;
    tate = 80;
  }
  
  void l_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw pants 1(back)*/
    rect(x,y-tate*0.5/22,yoko,tate*1.65/5);
    
    /*draw belt*/
    quad(x-(yoko/2),y-tate/5,x-((yoko*9/10)/2-2),y-tate*15/50,x+((yoko*9/10)/2-2),y-tate*15/50,x+((yoko/2)),y-tate/5);
    /*draw pants 2*/
    //draw left pant
    quad(x-yoko/2,y-tate/5,x-yoko*1.05/2,y+tate*5.6/50,x-yoko/20,y+tate*5/50,x-yoko/50,y+tate/8);
    quad(x-yoko*1.05/2,y+tate*5.6/50,x-yoko*1.1/3,y+tate*3.3/3,x-yoko/20,y+tate*3.3/3,x-yoko/20,y+tate*5/50);
    quad(x-yoko*1.1/3,y+tate*3.3/3,x-yoko*1.3/3,y+tate*4.1/3,x-yoko*1.4/20,y+tate*4.1/3,x-yoko/20,y+tate*3.3/3);
    quad(x-yoko*1.3/3,y+tate*4.1/3,x-yoko*1/3,y+tate*2,x-yoko*3.3/20,y+tate*2,x-yoko*1.4/20,y+tate*4.1/3);
    quad(x-yoko*1/3,y+tate*2,x-yoko*0.9/3,y+tate*2.5,x-yoko*2.7/20,y+tate*2.5,x-yoko*3.3/20,y+tate*2);
    quad(x-yoko*0.9/3,y+tate*2.5,x-yoko*1.25/3,y+tate*2.75,x-yoko*2.2/20,y+tate*2.75,x-yoko*2.7/20,y+tate*2.5);
    quad(x-yoko*1.25/3,y+tate*2.75,x-yoko*1.2/3,y+tate*2.87,x-yoko*2.7/20,y+tate*2.87,x-yoko*2.2/20,y+tate*2.75);
    arc(x-((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
  
    //draw right pant
    quad(x+yoko/2,y-tate/5,x+yoko*1.05/2,y+tate*5.6/50,x+yoko/20,y+tate*5/50,x+yoko/50,y+tate/8);
    quad(x+yoko*1.05/2,y+tate*5.6/50,x+yoko*1/3,y+tate*3.3/3,x+yoko/20,y+tate*3.3/3,x+yoko/20,y+tate*5/50);
    quad(x+yoko*1/3,y+tate*3.3/3,x+yoko*1.2/3,y+tate*4.1/3,x+yoko*1.4/20,y+tate*4.1/3,x+yoko/20,y+tate*3.3/3);
    quad(x+yoko*1.2/3,y+tate*4.1/3,x+yoko*0.95/3,y+tate*2,x+yoko*3.3/20,y+tate*2,x+yoko*1.4/20,y+tate*4.1/3);
    quad(x+yoko*0.95/3,y+tate*2,x+yoko*1.05/3,y+tate*2.5,x+yoko*2.7/20,y+tate*2.5,x+yoko*3.3/20,y+tate*2);
    quad(x+yoko*1.05/3,y+tate*2.5,x+yoko*1.25/3,y+tate*2.75,x+yoko*2.2/20,y+tate*2.75,x+yoko*2.7/20,y+tate*2.5);
    quad(x+yoko*1.25/3,y+tate*2.75,x+yoko*1.2/3,y+tate*2.87,x+yoko*2.7/20,y+tate*2.87,x+yoko*2.2/20,y+tate*2.75);
    arc(x+((yoko*1.3/3+yoko/15)/2+2),y+tate*2.863,yoko*0.7/3,yoko/9,0,PI);
  
  
    /*disappear the lines*/
    stroke(R,G,B);
    strokeWeight(3);
    line(x-(yoko/2-2),y-(tate/5-2),x-(yoko/50+1),y+tate/8-1);
    line(x+(yoko/2-2),y-(tate/5-2),x+(yoko/50+1),y+(tate/8-1));
    strokeWeight(1);
    stroke(0);
    line(x-yoko/12,y+(tate*5.6/50+2),x+yoko/12,y+(tate*5.6/50+2));
    
    //disappear pants lines
    stroke(R,G,B);
    line(x-((yoko/2)-1),y-tate/5,x+((yoko/2)-1),y-tate/5);
    line(x-(yoko*1.1/2-2),y+tate*5.6/50,x+(yoko*1.1/2-2),y+tate*5.6/50);
  
    //right
    line(x+(yoko*1/3-1),y+tate*3.3/3,x+(yoko/20+1),y+tate*3.3/3);
    line(x+(yoko*1.2/3-1),y+tate*4.1/3,x+(yoko*1.4/20+1),y+tate*4.1/3);
    line(x+(yoko*0.95/3-1),y+tate*2,x+(yoko*3.3/20+1),y+tate*2);
    line(x+(yoko*1.05/3-1),y+tate*2.5,x+(yoko*2.7/20+1),y+tate*2.5);
    line(x+(yoko*1.25/3-1),y+tate*2.75,x+(yoko*2.2/20+1),y+tate*2.75);
    
    //left
    line(x-(yoko*1.1/3-1),y+tate*3.3/3,x-(yoko/20+1),y+tate*3.3/3);
    line(x-(yoko*1.3/3-1),y+tate*4.1/3,x-(yoko*1.4/20+1),y+tate*4.1/3);
    line(x-(yoko*1/3-1),y+tate*2,x-(yoko*3.3/20+1),y+tate*2);
    line(x-(yoko*0.9/3-1),y+tate*2.5,x-(yoko*2.7/20+1),y+tate*2.5);
    line(x-(yoko*1.25/3-1),y+tate*2.75,x-(yoko*2.2/20+1),y+tate*2.75);
    
    Gara(pattern);
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)(y+75),110,220);
         p1.b_display(8,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)(y+110),50,285);
         p1.s_display(6,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)(y+75),50,220);
         p1.f_display(6,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)(y+75),50,220);
         p1.h_display(5,1);
         break;
      default:
        break;
      }
   }
  
  
}
    
