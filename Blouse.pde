//Written by Amai

class Blouse{
  float x;
  float y;
  float yoko;
  float tate;
  
  Blouse(int hx,int hy){
    x    = hx*195/200;
    y    = hy*220/300;
    yoko = 90;
    tate = 100;
  }
  
  void eri_s_display(float R,float G,float B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Blouse with collar*/
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.5,y+tate/8,x-(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2.5,y+tate/8);
    arc(x,y+tate/2,(yoko*3/5),yoko/7,0,PI);
    quad(x-yoko*3/10,y-tate/2,x-yoko*4/10,y-tate*4.5/10,x-yoko/2,y-tate*3/10,x-yoko*3/10,y-tate*2/10);
    quad(x+yoko*3/10,y-tate/2,x+yoko*4/10,y-tate*4.5/10,x+yoko/2,y-tate*3/10,x+yoko*3/10,y-tate*2/10);
   
    
    /*disappear lines*/
    stroke(R,G,B);
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
 
    
    /*draw button*/
    stroke(0);
    line(x+yoko*3/60,y-tate/2,x+yoko*3/60,y+tate/2+yoko/7/2);
    for(float i=y-tate*45/100;i<y+tate/2;i+=tate*15/100){
      ellipse(x,i,yoko*2/60,yoko*2/60);
    }

    /*disappear the collar*/
    fill(254,237,217);
    triangle(x-yoko/6,y-(tate/2+0.5),x+yoko/6,y-(tate/2+0.5),x,y-tate*40/95);
    stroke(254,237,217);
    line(x-yoko/6,y-(tate/2+0.5),x+yoko/6,y-(tate/2+0.5));
    
    
    /*draw collar*/
    stroke(0);
    fill(R,G,B);
    quad(x-yoko*10/90,y-tate*55/95,x-yoko*16/90,y-tate*50/95,x-yoko*15/90,y-tate*35/95,x,y-tate*40/95);
    quad(x+yoko*10/90,y-tate*55/95,x+yoko*16/90,y-tate*50/95,x+yoko*15/90,y-tate*35/95,x,y-tate*40/95);   
  
    Gara(pattern);
  }
  
  void s_display(float R,float G,float B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Blouse*/
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3.6/5)/2,y+tate/2,x+(yoko*3.6/5)/2,y+tate/2,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    for(float i=x-(yoko*3/5)/1.9;i<x+(yoko*3/5)/1.7;i+=(yoko*3/5)/5.7){
      arc(i,y+tate/2,(yoko*3/5)/6.2,yoko/7,0,PI);
    }
    quad(x-yoko*3/10,y-tate/2,x-yoko*4/10,y-tate*4.5/10,x-yoko/2,y-tate*3/10,x-yoko*3/10,y-tate*2/10);
    quad(x+yoko*3/10,y-tate/2,x+yoko*4/10,y-tate*4.5/10,x+yoko/2,y-tate*3/10,x+yoko*3/10,y-tate*2/10);
   
   
    /*disappear lines*/
    stroke(R,G,B);
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
 
    
    /*draw the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*5.5/10,tate*3/10,0, PI);
    
     Gara(pattern);
  }
  
  void eri_l_display(float R,float G,float B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Blouse with collar*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5/10),y+(tate*1.5/2),x-(yoko*3.8/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5/10),y+(tate*1.5/2),x+(yoko*3.8/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    
    //draw body
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2.5,y+tate/8,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    quad(x-(yoko*3/5)/2.5,y+tate/8,x-(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2,y+tate/2,x+(yoko*3/5)/2.5,y+tate/8);
    arc(x,y+tate/2,(yoko*3/5),yoko/7,0,PI);
    
    
    /*disappear lines*/
    stroke(R,G,B);
    //disappear body lines
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
 
    //disappear arm lines
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+3.5),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+2),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+3.3),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+2),y+(tate/4));
    
    
    /*draw button*/
    stroke(0);
    line(x+yoko*3/60,y-tate/2,x+yoko*3/60,y+tate/2+yoko/7/2);
    for(float i=y-tate*45/100;i<y+tate/2;i+=tate*15/100){
      ellipse(x,i,yoko*2/60,yoko*2/60);
    }

    /*disappear the collar*/
    fill(254,237,217);
    triangle(x-yoko/6,y-(tate/2+0.5),x+yoko/6,y-(tate/2+0.5),x,y-tate*40/95);
    stroke(254,237,217);
    line(x-yoko/6,y-(tate/2+0.5),x+yoko/6,y-(tate/2+0.5));
    
    
    /*draw collar*/
    stroke(0);
    fill(R,G,B);
    quad(x-yoko*10/90,y-tate*55/95,x-yoko*16/90,y-tate*50/95,x-yoko*15/90,y-tate*35/95,x,y-tate*40/95);
    quad(x+yoko*10/90,y-tate*55/95,x+yoko*16/90,y-tate*50/95,x+yoko*15/90,y-tate*35/95,x,y-tate*40/95);
    
     Gara(pattern);
    
  }
  
  void l_display(float R,float G,float B,int pattern){
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw a Blouse*/
    //draw right arms
    quad(x-(yoko*3/10),y-(tate/2),x-(yoko*4/10),y-(tate*4.35/10),x-(yoko*4.2/10),y,x-(yoko*2.5/10),y);
    quad(x-(yoko*4.2/10),y,x-(yoko*4/10),y+(tate/4),x-(yoko*2.5/10),y+(tate/4),x-(yoko*2.5/10),y);
    quad(x-(yoko*4/10),y+(tate/4),x-(yoko*5/10),y+(tate*1.5/2),x-(yoko*3.8/10),y+(tate*1.6/2),x-(yoko*2.5/10),y+(tate/4));
    
    //draw left arms
    quad(x+(yoko*3/10),y-(tate/2),x+(yoko*4/10),y-(tate*4.36/10),x+(yoko*4.2/10),y,x+(yoko*2.5/10),y);
    quad(x+(yoko*4.2/10),y,x+(yoko*4/10),y+(tate/4),x+(yoko*2.5/10),y+(tate/4),x+(yoko*2.5/10),y);
    quad(x+(yoko*4/10),y+(tate/4),x+(yoko*5/10),y+(tate*1.5/2),x+(yoko*3.8/10),y+(tate*1.6/2),x+(yoko*2.5/10),y+(tate/4));
    
    //draw body
    rect(x,y-(yoko*3.3/10),yoko*3/5,tate/2.5);
    quad(x-(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2,x-(yoko*3.6/5)/2,y+tate/2,x+(yoko*3.6/5)/2,y+tate/2,x+(yoko*3/5)/2,y-(yoko*3.3/10)+(tate/2.5)/2);
    for(float i=x-(yoko*3/5)/1.9;i<x+(yoko*3/5)/1.7;i+=(yoko*3/5)/5.7){
      arc(i,y+tate/2,(yoko*3/5)/6.2,yoko/7,0,PI);
    }
   
    /*disappear lines*/
    stroke(R,G,B);
    //disappear body lines
    line(x-yoko*3/10,y-(tate*2/10+5),x-yoko*3/10,y-(tate/2-1));
    line(x+yoko*3/10,y-(tate*2/10+5),x+yoko*3/10,y-(tate/2-1));
    line(x-(yoko*3/5)/2+1,y-(yoko*3.3/10)+(tate/2.5)/2,x+(yoko*3/5)/2-1,y-(yoko*3.3/10)+(tate/2.5)/2);
    line(x-(yoko*3/5)/2.5+1,y+tate/8,x+(yoko*3/5)/2.5-1,y+tate/8);
    
    //disappear arm lines
    line(x-((yoko*4.2/10)-1),y,x-((yoko*2.5/10)+6),y);
    line(x-((yoko*4/10)-1),y+(tate/4),x-((yoko*2.5/10)+8.5),y+(tate/4));
    line(x+((yoko*4.2/10)-1),y,x+((yoko*2.5/10)+6.5),y);
    line(x+((yoko*4/10)-1),y+(tate/4),x+((yoko*2.5/10)+8.5),y+(tate/4));
    
    /*draw the collar*/
    stroke(0);
    fill(254,237,217);
    arc(x,y-tate/2,yoko*5.5/10,tate*3/10,0, PI);
    
     Gara(pattern);
  }
  
  void Gara(int pattern){
    Pattern p1;
        switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)x,(int)y,55,100);
         p1.b_display(1,2);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)x,(int)y,55,95);
         p1.s_display(1,2);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)x,(int)y,60,100);
         p1.f_display(1,2);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)x,(int)y,50,100);
         p1.h_display(1,2);
         break;
      default:
        break;
    }
  }
  
  
}
    
