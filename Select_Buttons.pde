//Written by Ayano

int gSelectNumber;
int gHieralcy;//select Button no kaisou 
int gFromHieralcy;

//TextFiles
PrintWriter Text[]     = new PrintWriter [100];
PrintWriter TextNumber;
int gTextNumber;


void Buttons() {
  PFont ButtonFont = loadFont("SelectButton.vlw");
  textFont(ButtonFont, 10);
  
  textSize(40);
  fill(230, 100, 150);
  text(" Return Press 'r' key", width/2+80, height-30);

  switch(gHieralcy) {
  case 0:
    First();
    break;

  case 1:
    Tops();
    break;

  case 2:
    Outer();
    break;

  case 3:
    Pants();
    break;

  case 4:
    Skirt();
    break;

  case 5:
    Onepece();
    break;

  case 6:
    Shoes();
    break;

  case 7:
    Hats();
    break;

  case 8:
    Eri();
    break;

  case 9:
    gSelectNumber += 1*100;
    gHieralcy = 10;
    break;

  case 10:
    Gara();
    break;

  case 11:
    Color();
    Save();
    break;

  case 12:
    Socks();
    break;

  case 13:
    SodeTake();
    break;
  }
}

void First() {
  Tops    = new Button(width/2+80, height-530, 200, 40, 40, "Tops", color(255, 0, 0), color(0, 0, 255));
  Tops.Disp();
  Outer   = new Button(width/2+80, height-470, 200, 40, 40, "Outer", color(255, 0, 0), color(0, 0, 255));
  Outer.Disp();
  Pants   = new Button(width/2+80, height-410, 200, 40, 40, "Pants", color(255, 0, 0), color(0, 0, 255));
  Pants.Disp();
  Skirt   = new Button(width/2+80, height-350, 200, 40, 40, "Skirt", color(255, 0, 0), color(0, 0, 255));
  Skirt.Disp();
  Onepece = new Button(width/2+80, height-290, 200, 40, 40, "Onepiece", color(255, 0, 0), color(0, 0, 255));
  Onepece.Disp();
  Shoes   = new Button(width/2+80, height-230, 200, 40, 40, "Shoes", color(255, 0, 0), color(0, 0, 255));
  Shoes.Disp();
  Hats    = new Button(width/2+80, height-170, 200, 40, 40, "Hats", color(255, 0, 0), color(0, 0, 255));
  Hats.Disp();
  Socks   = new Button(width/2+80, height-110, 200, 40, 40, "Socks", color(255, 0, 0), color(0, 0, 255));
  Socks.Disp();

  if (Tops.mButtonPressed) {
    gHieralcy = 1;
    gSelectNumber = 1;
  }
  else if (Outer.mButtonPressed) {
    gHieralcy = 2;
    gSelectNumber = 2;
  }
  else if (Pants.mButtonPressed) {
    gHieralcy = 3;
    gSelectNumber = 3;
  }
  else if (Skirt.mButtonPressed) {
    gHieralcy = 4;
    gSelectNumber = 4;
  }
  else if (Onepece.mButtonPressed) {
    gHieralcy = 5;
    gSelectNumber = 5;
  }
  else if (Shoes.mButtonPressed) {
    gHieralcy = 6;
    gSelectNumber = 6;
  }
  else if (Hats.mButtonPressed) {
    gHieralcy = 7;
    gSelectNumber = 7;
  }
  else if (Socks.mButtonPressed) {
    gHieralcy     = 12;
    gSelectNumber = 8;
  }
  if (keyPressed && key == 'r') {  
    setup();
  }
 
}

void Tops() {
  TShirts   = new Button(width/2+30, height-460, 200, 40, 40, "T-Shirt", color(255, 0, 0), color(0, 0, 255));
  TShirts.Disp();
  Blaus     = new Button(width/2+30, height-400, 200, 40, 40, "Blouse", color(255, 0, 0), color(0, 0, 255));
  Blaus.Disp();
  Knit      = new Button(width/2+30, height-340, 200, 40, 40, "Knit", color(255, 0, 0), color(0, 0, 255));
  Knit.Disp();
  Kirdigan  = new Button(width/2+30, height-280, 200, 40, 40, "Cardigan", color(255, 0, 0), color(0, 0, 255));
  Kirdigan.Disp();
  Parker    = new Button(width/2+30, height-220, 200, 40, 40, "Parka", color(255, 0, 0), color(0, 0, 255));
  Parker.Disp();

  if (TShirts.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*100;
    gFromHieralcy = 1;
    gHieralcy = 13;
  }
  else if (Blaus.mButtonPressed) {
    gSelectNumber += 2*10;
    gHieralcy = 13;
    gFromHieralcy = 1;
  }
  else if (Knit.mButtonPressed) {
    gSelectNumber += 3*10;
    gSelectNumber += 1*1000;
    gFromHieralcy = 1;
    gHieralcy = 8;
  }
  else if (Kirdigan.mButtonPressed) {
    gHieralcy = 9;
    gFromHieralcy = 1;
    gSelectNumber += 4*10;
    gSelectNumber += 1*1000;
  }
  else if (Parker.mButtonPressed) {
    gHieralcy = 13;
    gFromHieralcy = 1;
    gSelectNumber += 5*10;
    gSelectNumber += 1*100;
  }
  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

void Outer() {
  Jacket = new Button(width/2+30, height-500, 200, 40, 40, "Jacket", color(255, 0, 0), color(0, 0, 255));
  Jacket.Disp();
  Coat   = new Button(width/2+30, height-440, 200, 40, 40, "Coat", color(255, 0, 0), color(0, 0, 255));
  Coat.Disp();
  Trench = new Button(width/2+30, height-380, 200, 40, 40, "Trench", color(255, 0, 0), color(0, 0, 255));
  Trench.Disp();
  Poncho = new Button(width/2+30, height-320, 200, 40, 40, "Poncho", color(255, 0, 0), color(0, 0, 255));
  Poncho.Disp();
  if (Jacket.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (Coat.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (Trench.mButtonPressed) {
    gSelectNumber += 3*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (Poncho.mButtonPressed) {
    gSelectNumber += 4*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }

  if (keyPressed && key == 'r') {
     gHieralcy     = 0;
     gSelectNumber = 0;
  }
}

void Pants() {
  LongPants  =new Button(width/2+30, height-500, 200, 40, 40, "LongPants", color(255, 0, 0), color(0, 0, 255));
  LongPants.Disp();
  ShortPants =new Button(width/2+30, height-440, 200, 40, 40, "ShortPants", color(255, 0, 0), color(0, 0, 255));
  ShortPants.Disp();
  if (LongPants.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (ShortPants.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 2*1000;
    gHieralcy = 9;
  }
  if (keyPressed && key == 'r') {
     gHieralcy     = 0;
     gSelectNumber = 0;
  }
}

void Skirt() {
  LongSkirt  =new Button(width/2+30, height-500, 200, 40, 40, "LongSkirt", color(255, 0, 0), color(0, 0, 255));
  LongSkirt.Disp();
  ShortSkirt =new Button(width/2+30, height-440, 200, 40, 40, "ShortSkirt", color(255, 0, 0), color(0, 0, 255));
  ShortSkirt.Disp();
  if (LongSkirt.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (ShortSkirt.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 2*1000;
    gHieralcy = 9;
  }
  if (keyPressed && key == 'r') {
      gHieralcy     = 0;
      gSelectNumber = 0;
  }
}

void Onepece() {
  Onepece  =new Button(width/2+30, height-500, 200, 40, 40, "Onepiece", color(255, 0, 0), color(0, 0, 255));
  Onepece.Disp();
  Chunick =new Button(width/2+30, height-440, 200, 40, 40, "Tunic", color(255, 0, 0), color(0, 0, 255));
  Chunick.Disp();
  if (Onepece.mButtonPressed) {
    gSelectNumber += 1*10;
    gHieralcy = 13;
    gFromHieralcy = 5;
  }
  if (Chunick.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 1*100;
    gHieralcy = 13;
  }
  if (keyPressed && key == 'r') {
      gHieralcy     = 0;
      gSelectNumber = 0;
  }
}
void Shoes() {
  Sneeker    =new Button(width/2+30, height-500, 200, 40, 40, "Sneaker", color(255, 0, 0), color(0, 0, 255));
  Sneeker.Disp();
  Punps      =new Button(width/2+30, height-440, 200, 40, 40, "Pumps", color(255, 0, 0), color(0, 0, 255));
  Punps.Disp();
  ShortBoots =new Button(width/2+30, height-380, 200, 40, 40, "ShortBoots", color(255, 0, 0), color(0, 0, 255));
  ShortBoots.Disp();
  LongBoots  =new Button(width/2+30, height-320, 200, 40, 40, "LongBoots", color(255, 0, 0), color(0, 0, 255));
  LongBoots.Disp();

  if (Sneeker.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (Punps.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (ShortBoots.mButtonPressed) {
    gSelectNumber += 3*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (LongBoots.mButtonPressed) {
    gSelectNumber += 4*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

void Hats() {
  KnitHat  = new Button(width/2+30, height-500, 200, 40, 40, "KnitCap", color(255, 0, 0), color(0, 0, 255));
  KnitHat.Disp();
  Berey    = new Button(width/2+30, height-440, 200, 40, 40, "Beret", color(255, 0, 0), color(0, 0, 255));
  Berey.Disp();
  if (KnitHat.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (Berey.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

void Socks() {
  Tights     = new Button(width/2+100, height-500, 200, 40, 40, "Tights", color(255, 0, 0), color(0, 0, 255));
  Tights.Disp();
  KneeSocks  = new Button(width/2+100, height-440, 200, 40, 40, "KneeSocks", color(255, 0, 0), color(0, 0, 255));
  KneeSocks.Disp();
  NormalSocks= new Button(width/2+100, height-380, 200, 40, 40, "Socks", color(255, 0, 0), color(0, 0, 255));
  NormalSocks.Disp();
  if (Tights.mButtonPressed) {
    gSelectNumber += 1*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (KneeSocks.mButtonPressed) {
    gSelectNumber += 2*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  } 
  if (NormalSocks.mButtonPressed) {
    gSelectNumber += 3*10;
    gSelectNumber += 1*1000;
    gHieralcy = 9;
  }
  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

void Eri() {
  EriTsuki = new Button(width/2+100, height-250, 200, 40, 40, "WithCollor", color(255, 0, 0), color(0, 0, 255));
  EriTsuki.Disp();
  EriNashi = new Button(width/2+100, height-190, 200, 40, 40, "WithoutCollor", color(255, 0, 0), color(0, 0, 255));
  EriNashi.Disp();

  if (EriTsuki.mButtonPressed) {
    gSelectNumber += 2*100;
    gHieralcy      = 10;
  }
  if (EriNashi.mButtonPressed) {
    gHieralcy = 9;
  }
  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

void Gara() {
  Muji   = new Button(width/2+100, height-530, 200, 40, 40, "Plain", color(255, 0, 0), color(0, 0, 255));
  Muji.Disp();
  Border = new Button(width/2+100, height-460, 200, 40, 40, "Border", color(255, 0, 0), color(0, 0, 255));
  Border.Disp();
  Stripe = new Button(width/2+100, height-400, 200, 40, 40, "Stripe", color(255, 0, 0), color(0, 0, 255));
  Stripe.Disp();
  Flower = new Button(width/2+100, height-320, 200, 40, 40, "Flower", color(255, 0, 0), color(0, 0, 255));
  Flower.Disp();
  Heart  = new Button(width/2+100, height-260, 200, 40, 40, "Heart", color(255, 0, 0), color(0, 0, 255));
  Heart.Disp();

  if (Muji.mButtonPressed) {
    gSelectNumber += 1*10000;
    gHieralcy = 11;
  }
  if (Border.mButtonPressed) {
    gSelectNumber += 2*10000;
    gHieralcy = 11;
  }
  if (Stripe.mButtonPressed) {
    gSelectNumber += 3*10000;
    gHieralcy = 11;
  }
  if (Flower.mButtonPressed) {
    gSelectNumber += 4*10000;
    gHieralcy = 11;
  }
  if (Heart.mButtonPressed) {
    gSelectNumber += 5*10000;
    gHieralcy = 11;
  }

  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

void Color() {
  if (key == 'r') {
   if(gKeyRelease){
    gHieralcy     = 0;
    gSelectNumber = 0;
   }
  }
  Palette.display_color();
  ClothPreview();
}


void SodeTake() {
  NagaSode = new Button(width/2+150, height-500, 200, 40, 40, "LongSleeves", color(255, 0, 0), color(0, 0, 255));
  NagaSode.Disp();
  HanSode  = new Button(width/2+150, height-440, 200, 40, 40, "ShortSleeves", color(255, 0, 0), color(0, 0, 255));
  HanSode.Disp();

  if (NagaSode.mButtonPressed) {
    gSelectNumber += 1000*1;
    if((int)(gSelectNumber/100)%10 == 1){
       gHieralcy = 10;
    }
    else{
       gHieralcy = 8;
    }
  }
  if (HanSode.mButtonPressed) {
    gSelectNumber += 1000*2;
    if((int)(gSelectNumber/100)%10 == 1){
       gHieralcy = 10;
    }
    else{
       gHieralcy = 8;
    }
  }
  if (keyPressed && key == 'r') {
    gHieralcy     = 0;
    gSelectNumber = 0;
  }
}

int     aTimer;
boolean aTimerFlag;
void Save() {
  textSize(40);
  fill(0);
  text("Let's Save by Press 's' key", width/2+80, height-500);
  fill(255,0,0);
  text("!NOT Hit Repeatedly!", width/2+80, height-450);
  if (key == 's') {
    if (gKeyRelease) {
      println("save");
      Text[gTextNumber] = createWriter("cloth"+str(gTextNumber)+".txt");
      Text[gTextNumber].println(String.format("%1$03d", Palette.R)+String.format("%1$03d", Palette.G)+String.format("%1$03d", Palette.B)+str(gSelectNumber));
      Text[gTextNumber].flush();
      Text[gTextNumber].close();
      gTextNumber++;
      TextNumber = createWriter("TextNumber.txt");
      TextNumber.println(gTextNumber);
      TextNumber.flush();
      TextNumber.close();
      aTimerFlag = true;
    }
  }

  if (aTimerFlag) {
    aTimer++;
    if (aTimer <= 5) {
      save();
    } 
    else {
      aTimerFlag  = false;
      aTimer      = 0;
      setup();
    }
  }
}

