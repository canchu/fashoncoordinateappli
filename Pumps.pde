//Written by Amai

class Pumps{
  float x;
  float y;
  float tate;
  float yoko;

  Pumps(int hx,int hy){
    x    = hx*196/200;
    y    = hy*285/300;
    yoko = 59.5;
    tate = 80;
  }
  
  void s_display(int R,int G,int B,int pattern){
    strokeWeight(1);
    stroke(0);
    fill(R,G,B);
    rectMode(CENTER);
    
    /*draw pumps*/
    arc(x-((yoko*1.3/3+yoko/15)/2+1),y+tate*2.8,yoko*0.8/3,yoko/3,-PI*0.5/4,PI*4.5/4);
    arc(x+((yoko*1.3/3+yoko/15)/2+1),y+tate*2.8,yoko*0.8/3,yoko/3,-PI*0.5/4,PI*4.5/4);  
  Gara(pattern);
}
  
  void Gara(int pattern)
  {
      Pattern p1;
      Pattern p2;
      
      switch (pattern){
      case 1:
        println("muji");
        break;
      case 2:
        println("border");
         p1 = new Pattern((int)(x-15),(int)(y+217),15,10);
         p2 = new Pattern((int)(x+15),(int)(y+217),15,10);
         p1.b_display(4,1);
         p2.b_display(4,1);
        break;
      case 3:
        println("stripe");
         p1 = new Pattern((int)(x-15),(int)(y+217),15,10);
         p2 = new Pattern((int)(x+15),(int)(y+217),15,10);
         p1.s_display(4,1);
         p2.s_display(4,1);
        break;
       case 4:
         println("flower");
         p1 = new Pattern((int)(x-15),(int)(y+215),15,15);
         p2 = new Pattern((int)(x+15),(int)(y+215),15,15);
         p1.f_display(4,1);
         p2.f_display(4,1);
         break;
       case 5:
         println("heart");
         p1 = new Pattern((int)(x-15),(int)(y+215),15,15);
         p2 = new Pattern((int)(x+15),(int)(y+215),15,15);
         p1.h_display(4,1);
         p2.h_display(4,1);
         break;
      default:
        break;
      }
  }
}


 
